!----------------------------------------
!     Number of scalars (fix to 9)
integer ndim
parameter(ndim=9)
!----------------------------------------
!     Number of sites along t-direction
integer nsite
parameter(nsite=16)
!----------------------------------------
!     Size of matrices
integer nmat
parameter(nmat=8)
