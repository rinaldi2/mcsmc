subroutine Calc_action(nmat,ndim,nsite,nbc,temperature,xmat,umat,action)

  implicit none

  integer nmat,ndim,nsite,nbc
  double complex xmat(1:nmat,1:nmat,1:ndim,1:nsite)
  double complex umat(1:nmat,1:nmat,1:nsite)
  double precision action,kinetic,potential
  double precision temperature,lattice_spacing

  double complex commutator(1:nmat,1:nmat),ux(1:nmat,1:nmat),&
       uxumx(1:nmat,1:nmat)
  integer isite,isite_p1
  integer idim,jdim
  integer imat,jmat,kmat

  lattice_spacing=1d0/temperature/dble(nsite)

  !potential term
  potential=0d0
  do isite=1,nsite
     do idim=1,ndim-1
        do jdim=idim+1,ndim
           commutator=(0d0,0d0)
           do imat=1,nmat
              do jmat=1,nmat
                 do kmat=1,nmat
                    commutator(imat,jmat)=commutator(imat,jmat)&
                         +xmat(imat,kmat,idim,isite)*xmat(kmat,jmat,jdim,isite)&
                         -xmat(imat,kmat,jdim,isite)*xmat(kmat,jmat,idim,isite)
                 end do
              end do
           end do
           do imat=1,nmat
              do jmat=1,nmat
                 potential=potential&
                      +dble(commutator(imat,jmat)*dconjg(commutator(imat,jmat)))
              end do
           end do

        end do
     end do
  end do
  potential=potential*0.5d0*dble(nmat)*lattice_spacing

  !kinetic term
  kinetic=0d0
  do isite=1,nsite
     
     if(isite.LT.nsite)then
        isite_p1=isite+1
     else
        isite_p1=1
     end if
     
     do idim=1,ndim
        !u(t)*x(t+a)
        ux=(0d0,0d0)
        do imat=1,nmat
           do jmat=1,nmat
              do kmat=1,nmat
                 ux(imat,jmat)=ux(imat,jmat)&
                      +umat(imat,kmat,isite)*xmat(kmat,jmat,idim,isite_p1)
              end do
           end do
        end do
        !u(t)*x(t+a)*u^dagger(t) - x(t)
        do imat=1,nmat
           do jmat=1,nmat
              uxumx(imat,jmat)=(-1d0,0d0)*xmat(imat,jmat,idim,isite)
           end do
        end do
        do imat=1,nmat
           do jmat=1,nmat
              do kmat=1,nmat
                 uxumx(imat,jmat)=uxumx(imat,jmat)&
                      +ux(imat,kmat)*dconjg(umat(jmat,kmat,isite))
              end do
           end do
        end do
        
        do imat=1,nmat
           do jmat=1,nmat
              kinetic=kinetic+dble(uxumx(imat,jmat)*uxumx(jmat,imat))
           end do
        end do
        
     end do
  end do
  
  
  kinetic=kinetic*0.5d0*dble(nmat)/lattice_spacing
  action=kinetic+potential


  return

END subroutine Calc_action
