!************************************************
!*** Calculate trx2 = (1/N)*¥int dt Tr(X_I^2) ***
!************************************************
subroutine Calc_TrX(xmat,trx,nprocs,myrank)

  implicit none

  include 'mpif.h'
  include 'size_parallel.h'

  integer nprocs,myrank
  double complex xmat(1:nmat,1:nmat,1:ndim,-(nmargin-1):nsite_local+nmargin)
  double precision trx(1:ndim,1:nsite_local)

  integer isite,idim
  integer imat,jmat
  integer IERR

  trx=0d0
  do isite=1,nsite_local
     do idim=1,ndim
        do imat=1,nmat
           trx(idim,isite)=trx(idim,isite)&
                +dble(xmat(imat,imat,idim,isite))
        end do
     end do
  end do


  !The final result is stored at myrank=0.

  return

END subroutine Calc_TrX
