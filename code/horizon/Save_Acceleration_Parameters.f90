  !*************************************
  !*** Save the final configuration ****
  !*************************************


SUBROUTINE Save_Acceleration_Parameters(fluctuation,imeasure,acc_output)
  
  use mtmod !Mersenne twistor
  implicit none
  
  include 'size_parallel.h'
  include 'mpif.h'
  include 'unit_number.inc'
  !---------------------------------
  double complex xmat_mom(1:nmat,1:nmat,1:ndim,1:nsite_local) 
  double precision fluctuation(1:nsite_local) 
  integer imeasure
  double precision, allocatable :: acceleration_fin(:)
  character(1000) acc_output

  integer myrank,nprocs,IERR

  call MPI_COMM_SIZE(MPI_COMM_WORLD,NPROCS, IERR)
  call MPI_COMM_RANK(MPI_COMM_WORLD,MYRANK, IERR)

  if(myrank.eq.0)then
     allocate(acceleration_fin(1:nsite_local*nprocs))
  end if

  call Fourier_acceleration_optimize(xmat_mom,fluctuation,&
       &nprocs,myrank,imeasure,2)
  
  call MPI_Gather(fluctuation(1),nsite_local,&
       &MPI_DOUBLE_PRECISION,&
       &acceleration_fin(1),nsite_local,MPI_DOUBLE_PRECISION,&
       &0,MPI_COMM_WORLD,IERR)


  if(myrank.eq.0)then

     open(UNIT=unit_output_acc, File = acc_output, STATUS = "REPLACE", ACTION = "WRITE")
     write(unit_output_acc,*) acceleration_fin
     close(unit_output_acc)
     deallocate(acceleration_fin)
  end if

  return

END SUBROUTINE Save_Acceleration_Parameters
