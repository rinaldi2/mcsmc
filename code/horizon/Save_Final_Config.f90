  !*************************************
  !*** Save the final configuration ****
  !*************************************


SUBROUTINE Save_Final_Config(xmat,alpha,itraj,output_config)
  
  use mtmod !Mersenne twistor
  implicit none
  
  include 'size_parallel.h'
  include 'mpif.h'
  include 'unit_number.inc'
  !---------------------------------
  double precision alpha(1:nmat)
  double complex xmat(1:nmat,1:nmat,1:ndim,-(nmargin-1):nsite_local+nmargin)
  integer itraj
  !final configuration
  double complex, allocatable :: xmat_fin(:,:,:,:)
  character(1000) output_config

  integer myrank,nprocs,IERR

  call MPI_COMM_SIZE(MPI_COMM_WORLD,NPROCS, IERR)
  call MPI_COMM_RANK(MPI_COMM_WORLD,MYRANK, IERR)

  if(myrank.eq.0)then
     allocate(xmat_fin(1:nmat,1:nmat,1:ndim,1:nsite_local*nprocs))
  end if
  !Gather xmat from all MPI processes to myrank=0
  call MPI_Gather(xmat(1,1,1,1),nmat*nmat*ndim*nsite_local,&
       &MPI_DOUBLE_COMPLEX,&
       &xmat_fin(1,1,1,1),nmat*nmat*ndim*nsite_local,MPI_DOUBLE_COMPLEX,&
       &0,MPI_COMM_WORLD,IERR)

  if(myrank.eq.0)then
     open(UNIT=unit_output_config, File = output_config, STATUS = "REPLACE", ACTION = "WRITE")
     call mtsaveu(unit_output_config)  
     write(unit_output_config,*) itraj
     write(unit_output_config,*) xmat_fin
     write(unit_output_config,*) alpha
     close(unit_output_config)
     deallocate(xmat_fin)
  end if

  return

END SUBROUTINE Save_Final_Config
