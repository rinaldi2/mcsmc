!************************************************
!*** Calculate trx2 = (1/N)*¥int dt Tr(X_I^2) ***
!************************************************
subroutine Calc_TrX2(xmat,trx2,nprocs,myrank)

  implicit none

  include 'mpif.h'
  include 'size_parallel.h'

  integer nprocs,myrank
  double complex xmat(1:nmat,1:nmat,1:ndim,-(nmargin-1):nsite_local+nmargin)
  double precision trx2,trx2_local

  integer isite,idim
  integer imat,jmat
  integer IERR

  trx2_local=0d0
  do isite=1,nsite_local
     do idim=1,ndim
        do jmat=1,nmat
           do imat=1,nmat
              trx2_local=trx2_local&
                   +dble(xmat(imat,jmat,idim,isite)&
                   *dconjg(xmat(imat,jmat,idim,isite)))
           end do
        end do
     end do
  end do

  call MPI_Reduce(trx2_local,trx2,1,MPI_DOUBLE_PRECISION,&
       MPI_SUM,0,MPI_COMM_WORLD,IERR)
  if(myrank.EQ.0)then
     trx2=trx2/dble(nmat*nsite_local*nprocs)
  end if

  !The final result is stored at myrank=0.

  return

END subroutine Calc_TrX2
