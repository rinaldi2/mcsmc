! Calculate the smallest eigenvalue of (D^dag*D),  
! by multiplying (D^dag*D)^{-1} many times to a random vector. 
SUBROUTINE Smallest_eigenvalue(nprocs,temperature,&
     &xmat,alpha,GAMMA10d,neig,smallest_eig,myrank,nbc,max_err,max_iteration,nbmn,flux)

  implicit none

  include 'mpif.h'
  include 'size_parallel.h'

  integer neig,ieig,nbc,nremez,info,max_iteration,iteration,nbmn
  double precision max_err

  double complex xmat(1:nmat,1:nmat,1:ndim,-(nmargin-1):nsite_local+nmargin)
  double precision alpha(1:nmat)
  double precision bcoeff(1:1)!use multimass CG solver, with nremez=1
  double complex phi1(1:nmat,1:nmat,1:nspin,-(nmargin-1):nsite_local+nmargin)
  double complex phi2(1:1,1:nmat,1:nmat,1:nspin,-(nmargin-1):nsite_local+nmargin)!use multimass CG solver, with nremez=1 and bcoeff=0
  double complex GAMMA10d(1:ndim,1:nspin,1:nspin)
  double complex trace
  double precision smallest_eig,norm,norm_local
  !pf2=M*pf1, M: Dirac op

  double precision temperature,flux
  integer imat,jmat
  integer ispin
  integer isite
  integer i

  integer nprocs,myrank,IERR

  double precision r1,r2

  !***********************************
  !**** generate a random vector. ****
  !***********************************
  if(myrank.GT.0)then
     !throw away some random numbers in order to 
     !synchronize with other nodes and avoid communication
     do i=1,nmat*nmat*nspin*nsite_local*myrank
        call BoxMuller(r1,r2)
     end do
  end if
  
  norm_local=0d0
  do imat=1,nmat     
     do jmat=1,nmat
        do ispin=1,nspin
           do isite=1,nsite_local
              call BoxMuller(r1,r2)
              phi1(imat,jmat,ispin,isite)=&
                   &(dcmplx(r1)+dcmplx(r2)*(0D0,1D0))/dcmplx(dsqrt(2d0))
              norm_local=norm_local+(r1*r1+r2*r2)*0.5d0
           end do
        end do
     end do
  end do
  !****************************
  !*** traceless projection ***
  !****************************
  do ispin=1,nspin
     do isite=1,nsite_local
        trace=(0d0,0d0)
        do imat=1,nmat
           trace=trace+phi1(imat,imat,ispin,isite)
        end do
        trace=trace/dcmplx(nmat)
        do imat=1,nmat
           norm_local=norm_local-dble(phi1(imat,imat,ispin,isite)*dconjg(phi1(imat,imat,ispin,isite)))
           phi1(imat,imat,ispin,isite)=phi1(imat,imat,ispin,isite)-trace
           norm_local=norm_local+dble(phi1(imat,imat,ispin,isite)*dconjg(phi1(imat,imat,ispin,isite)))
        end do
        
     end do
  end do

  if(myrank.LT.nprocs-1)then
     !throw away some random numbers in order to 
     !synchronize with other nodes and avoid communication
     do i=1,nmat*nmat*nspin*nsite_local*(nprocs-myrank-1)
        call BoxMuller(r1,r2)
     end do
  end if
  
  call MPI_Reduce(norm_local,norm,1,MPI_DOUBLE_PRECISION,&
       &MPI_SUM,0,MPI_COMM_WORLD,IERR)
  call MPI_Bcast(norm,1,MPI_DOUBLE_PRECISION,0,MPI_COMM_WORLD,IERR)
  
  norm=dsqrt(norm)
  norm=1d0/norm
  phi1=phi1*norm
  !*********************************
  !*** adjust the margin and b.c.***
  !*********************************
  call Adjust_margin_and_bc_pf(phi1,myrank,nprocs,nbc)
  !******************************************
  !*** random vector has been generated.  ***
  !******************************************
  nremez=1
  bcoeff(1)=0d0
  do ieig=1,neig
     !*****************************************************
     !*** phi1 -> phi2=(D^dag*D)^{-1}*phi1 -> phi1=phi2 ***
     !*****************************************************
     call solver_biCGm(nbc,nbmn,nremez,&
     xmat,alpha,phi1,phi2,GAMMA10d,&
     bcoeff,max_err,max_iteration,iteration,&
     temperature,flux,info)

!call solver_biCGm(nbc,nprocs,nremez,bcoeff,temperature,&
!     xmat,alpha,phi1,phi2,GAMMA10d,max_err,max_iteration,iteration,myrank,&
!     nbmn,flux,info)
     do imat=1,nmat     
        do jmat=1,nmat
           do ispin=1,nspin
              do isite=-(nmargin-1),nsite_local+nmargin
                 phi1(imat,jmat,ispin,isite)=phi2(1,imat,jmat,ispin,isite)
              end do
           end do
        end do
     end do
     !****************************************
     !*** traceless projection;            ***
     !*** to avoid the zero mode to appear ***
     !*** as numerical artifact            ***
     !****************************************
     ! (Practically, it does not seem to be necessary.) 
     !do ispin=1,nspin
     !   do isite=-nimprove,nsite_local+1+nimprove
     !      trace=(0d0,0d0)
     !      do imat=1,nmat
     !         trace=trace+phi1(imat,imat,ispin,isite)
     !      end do
     !      trace=trace/dcmplx(nmat)
     !      do imat=1,nmat
     !         phi1(imat,imat,ispin,isite)=phi1(imat,imat,ispin,isite)-trace
     !      end do
     !      
     !   end do
     !end do

     !************************************
     !*** calculate the norm of phi1.  ***
     !************************************
     norm_local=0d0
     do imat=1,nmat     
        do jmat=1,nmat
           do ispin=1,nspin
              do isite=1,nsite_local
                 norm_local=norm_local&
                      &+dble(dconjg(phi1(imat,jmat,ispin,isite)&
                      &*dconjg(phi1(imat,jmat,ispin,isite))))
              end do
           end do
        end do
     end do
     call MPI_Reduce(norm_local,norm,1,MPI_DOUBLE_PRECISION,&
          &MPI_SUM,0,MPI_COMM_WORLD,IERR)
     call MPI_Bcast(norm,1,MPI_DOUBLE_PRECISION,0,MPI_COMM_WORLD,IERR)
     norm=dsqrt(norm)
     norm=1d0/norm
     phi1=phi1*norm
     norm=1d0/norm
    
     !write(*,*)1d0/norm
  end do
  
  smallest_eig=1d0/norm

  return
  
END SUBROUTINE Smallest_eigenvalue
