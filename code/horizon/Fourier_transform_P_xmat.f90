!nxp=1 -> P_xmat to P_xmat_mom
!nxp=2 -> P_xmat_mom to P_xmat
subroutine Fourier_transform_P_xmat(P_xmat,P_xmat_mom,nprocs,myrank,nxp)

  implicit none

  include 'mpif.h'
  include 'size_parallel.h'

  integer nprocs,myrank,nxp
  double complex P_xmat(1:nmat,1:nmat,1:ndim,1:nsite_local)
  double complex P_xmat_mom(1:nmat,1:nmat,1:ndim,1:nsite_local)

  double complex, allocatable :: P_xmat_all(:,:,:,:)
  double complex, allocatable :: P_xmat_mom_all(:,:,:,:)
  
  double complex phase
  double precision temp,pi

  integer isite,idim,imom
  integer imat,jmat
  integer IERR

  pi=2d0*dasin(1d0)


 if (nxp.EQ.1)then
    allocate(P_xmat_all(1:nmat,1:nmat,1:ndim,1:nsite_local*nprocs))
    call MPI_Allgather(P_xmat(1,1,1,1),nmat*nmat*ndim*nsite_local,&
          MPI_DOUBLE_COMPLEX,&
          P_xmat_all(1,1,1,1),nmat*nmat*ndim*nsite_local,MPI_DOUBLE_COMPLEX,&
          MPI_COMM_WORLD,IERR)

     P_xmat_mom=(0d0,0d0)
     do imom=1,nsite_local
        do isite=1,nsite_local*nprocs
           temp=2d0*pi*dble((imom+myrank*nsite_local)*(isite))&
                &/dble(nprocs*nsite_local)
           phase=dcmplx(dcos(temp))-(0d0,1d0)*dcmplx(dsin(temp))
           phase=phase/dcmplx(dsqrt(dble(nprocs*nsite_local)))
           do idim=1,ndim
              do imat=1,nmat
                 do jmat=1,nmat
                    P_xmat_mom(imat,jmat,idim,imom)=&
                         &P_xmat_mom(imat,jmat,idim,imom)&
                         &+P_xmat_all(imat,jmat,idim,isite)*phase
                 end do
              end do
           end do
        end do
     end do

     deallocate(P_xmat_all)

  else if(nxp.EQ.2)then

     allocate(P_xmat_mom_all(1:nmat,1:nmat,1:ndim,1:nsite_local*nprocs))
     call MPI_Allgather(P_xmat_mom(1,1,1,1),nmat*nmat*ndim*nsite_local,&
          MPI_DOUBLE_COMPLEX,&
          P_xmat_mom_all(1,1,1,1),nmat*nmat*ndim*nsite_local,MPI_DOUBLE_COMPLEX,&
          MPI_COMM_WORLD,IERR)

     P_xmat=(0d0,0d0)
     do isite=1,nsite_local
        do imom=1,nsite_local*nprocs
           temp=2d0*pi*dble((isite+myrank*nsite_local)*(imom))&
                &/dble(nprocs*nsite_local)
           phase=dcmplx(dcos(temp))+(0d0,1d0)*dcmplx(dsin(temp))
           phase=phase/dcmplx(dsqrt(dble(nprocs*nsite_local)))
           do idim=1,ndim
              do imat=1,nmat
                 do jmat=1,nmat
                    P_xmat(imat,jmat,idim,isite)=&
                         &P_xmat(imat,jmat,idim,isite)&
                         &+P_xmat_mom_all(imat,jmat,idim,imom)*phase
                 end do
              end do
           end do
        end do
     end do
 
     deallocate(P_xmat_mom_all)
 
     
  end if

  return

END subroutine Fourier_transform_P_xmat
