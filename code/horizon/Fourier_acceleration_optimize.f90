! info=1 -> measure fluctuation
! info=2 -> set the acceleration parameter
subroutine Fourier_acceleration_optimize(xmat_mom,fluctuation,nprocs,myrank,imeasure,info)

  implicit none
  
  include 'mpif.h'
  include 'size_parallel.h'
  !***** input *****
  integer nprocs,myrank,info 
  !***** input, when info=1 *****
  double complex xmat_mom(1:nmat,1:nmat,1:ndim,1:nsite_local)
  !***** input & output *****
  double precision fluctuation(1:nsite_local)
  !***** input & output when info=1, input when info=2 *****
  integer imeasure !number of measurement performed.
  !************************
  double precision, allocatable :: fluctuation_all(:)
  double precision temp
  integer imom,imat,jmat,idim
  !***** for MPI *****
  integer IERR
  
  
  if(info.EQ.1)then
     !******************************************************************
     !*** measure fluctuation and add to the sum of previous values. ***
     !******************************************************************
     !fluctuation(p)=|Xmat_mom(p)|
     do imom=1,nsite_local
        do imat=1,nmat
           do jmat=1,nmat
              do idim=1,ndim
                 fluctuation(imom)=fluctuation(imom)&
                      +dble(xmat_mom(imat,jmat,idim,imom)&
                      *dconjg(xmat_mom(imat,jmat,idim,imom)))
              end do
           end do
        end do
     end do
     imeasure=imeasure+1

  else if(info.EQ.2)then

     fluctuation=fluctuation/dble(nmat*imeasure)
     if(myrank.EQ.0)then
        allocate(fluctuation_all(1:nsite_local*nprocs))
     end if
     call MPI_Gather(fluctuation(1),nsite_local,&
          MPI_DOUBLE_PRECISION,&
          fluctuation_all(1),nsite_local,MPI_DOUBLE_PRECISION,&
          0,MPI_COMM_WORLD,IERR)
     if(myrank.EQ.0)then
        !*************************************************************
        !*** fluctuation(p) and fluctuation(-p) should be averaged ***
        !*************************************************************
        do imom=1,int(dble(nprocs*nsite_local)*0.5d0+0.01d0)-1
           temp=fluctuation_all(imom)+fluctuation_all(nprocs*nsite_local-imom)
           temp=temp*0.5d0
           fluctuation_all(imom)=temp
           fluctuation_all(nprocs*nsite_local-imom)=temp
        end do
        !**********************************************************
        !*** Fourier acceleration parameter = sqrt(fluctuation) ***
        !**********************************************************
        fluctuation_all=dsqrt(fluctuation_all)
     end if
     call MPI_scatter(fluctuation_all(1),nsite_local,&
          MPI_DOUBLE_PRECISION,&
          fluctuation(1),nsite_local,MPI_DOUBLE_PRECISION,&
          0,MPI_COMM_WORLD,IERR)
     if(myrank.EQ.0)then
        deallocate(fluctuation_all)
     end if
  end if

  return

END subroutine Fourier_acceleration_optimize
