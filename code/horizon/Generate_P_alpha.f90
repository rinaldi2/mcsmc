! Generate P_alpha with Gaussian weight.
! We do not have to care about the traceless condition. 
SUBROUTINE Generate_P_alpha(P_alpha)
  
  implicit none
  include 'size_parallel.h'

  integer imat
  double precision r1,r2

  double precision P_alpha(1:nmat)
  
  !The same thing is done at all nodes, 
  !in order to synchronize random numbers
  !and avoid unnecessary communications. 
  do imat=1,nmat
     call BoxMuller(r1,r2)
     P_alpha(imat)=r1
  end do
  
  return
  
END SUBROUTINE Generate_P_alpha
