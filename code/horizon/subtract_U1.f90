!trace part of (¥int dt X) and alpha are removed. 
SUBROUTINE subtract_U1(xmat,alpha,nprocs)
  
  implicit none
  include 'mpif.h'
  include 'size_parallel.h'
  !***** input *****
  integer nprocs
  !***** input & output *****
  double complex xmat(1:nmat,1:nmat,1:ndim,-(nmargin-1):nsite_local+nmargin)
  double precision alpha(1:nmat)
  !****************************
  integer idim,imat,isite,ierr
  double complex trace(1:ndim),trace_local(1:ndim)
  double precision sum_alpha
  
  !***********************************************
  !**** trace part of (¥int dt X) is removed. ****
  !***********************************************
  trace_local=(0d0,0d0)!contribution from each MPI process.
  trace=(0d0,0d0)!sum of contributions from all processes.
  do idim=1,ndim
     do isite=1,nsite_local
        do imat=1,nmat
           trace_local(idim)=trace_local(idim)+xmat(imat,imat,idim,isite)
        end do
     end do
  end do
  call MPI_Allreduce(trace_local(1),trace(1),ndim,MPI_DOUBLE_COMPLEX,&
       MPI_SUM,MPI_COMM_WORLD,IERR)
  trace=trace/dcmplx(nsite_local*nprocs*nmat)
  do idim=1,ndim
     !take care of the margin too. 
     do isite=-(nmargin-1),nsite_local+nmargin
        do imat=1,nmat
           xmat(imat,imat,idim,isite)=xmat(imat,imat,idim,isite)-trace(idim)
        end do
     end do
  end do

  !*********************************************************
  !*** Trace part of alpha is removed. *********************
  !*** Do the same at all nodes, to avoid communication. ***
  !*********************************************************  
  sum_alpha=0d0
  do imat=1,nmat
     sum_alpha=sum_alpha+alpha(imat)
  end do
  sum_alpha=sum_alpha/dble(nmat)
  do imat=1,nmat
     alpha(imat)=alpha(imat)-sum_alpha
  end do
  
  return
  
END SUBROUTINE subtract_U1
