!This subroutine adjust the margins of xmat (isite < 1 and isite > nsite_local),
!by communicating with the neigbouring processes.
SUBROUTINE Adjust_margin_xmat(xmat,myrank,nprocs)

  implicit none
  include 'size_parallel.h'
  include 'mpif.h'
  !***** input *****
  integer nprocs,myrank
  !***** input & output *****
  double complex xmat(1:nmat,1:nmat,1:ndim,-(nmargin-1):nsite_local+nmargin)
  !***** for MPI *****
  integer send_rank,receive_rank,ireq,ierr,tag
  integer status(MPI_STATUS_SIZE)

  if(myrank.ne.nprocs-1)then
     send_rank=myrank+1
  else
     send_rank=0
  end if
  if(myrank.ne.0)then
     receive_rank=myrank-1
  else
     receive_rank=nprocs-1
  end if
  tag=1
  call MPI_Isend(xmat(1,1,1,nsite_local-nimprove),nmat*nmat*ndim*nmargin,&
       &MPI_DOUBLE_COMPLEX,&
       &send_rank,tag,MPI_COMM_WORLD,ireq,ierr)
  call MPI_Recv(xmat(1,1,1,-nimprove),nmat*nmat*ndim*nmargin,&
       &MPI_DOUBLE_COMPLEX,&
       &receive_rank,tag,MPI_COMM_WORLD,status,ierr)
  call MPI_Wait(ireq,status,ierr)
  !******
  if(myrank.ne.0)then
     send_rank=myrank-1
  else
     send_rank=nprocs-1
  end if
  if(myrank.ne.nprocs-1)then
     receive_rank=myrank+1
  else
     receive_rank=0
  end if
  tag=3
  call MPI_Isend(xmat(1,1,1,1),nmat*nmat*ndim*nmargin,&
       &MPI_DOUBLE_COMPLEX,&
       &send_rank,tag,MPI_COMM_WORLD,ireq,ierr)
  call MPI_Recv(xmat(1,1,1,nsite_local+1),nmat*nmat*ndim*nmargin,&
       &MPI_DOUBLE_COMPLEX,&
       &receive_rank,tag,MPI_COMM_WORLD,status,ierr) 
  call MPI_Wait(ireq,status,ierr)
  
  return

END SUBROUTINE Adjust_margin_xmat
!***************************************************************
!This subroutine adjust the margin of pseudo-fermion 
! (isite < 1 and isite > nsite_local), 
!by communicating with the neigbouring processes.
!It also properly sets the boundary condition (pbc or apbc).
SUBROUTINE Adjust_margin_and_bc_pf(pf,myrank,nprocs,nbc)
  
  implicit none
  include 'size_parallel.h'
  include 'mpif.h'
  !***** input *****
  integer nprocs,myrank,nbc
  !***** input & output *****
  double complex pf(1:nmat,1:nmat,1:nspin,-(nmargin-1):nsite_local+nmargin)
  !***** for MPI *****
  integer send_rank,receive_rank,ireq,ierr,tag
  integer status(MPI_STATUS_SIZE)
  !*******************
  integer isite,imat,jmat,ispin
  
  
  !*************************
  !*** adjust the margin ***
  !************************* 
  if(myrank.ne.nprocs-1)then
     send_rank=myrank+1
  else
     send_rank=0
  end if
  if(myrank.ne.0)then
     receive_rank=myrank-1
  else
     receive_rank=nprocs-1
  end if
  tag=1
  call MPI_Isend(pf(1,1,1,nsite_local-nimprove),nmat*nmat*nspin*nmargin,&
       &MPI_DOUBLE_COMPLEX,&
       &send_rank,tag,MPI_COMM_WORLD,ireq,ierr)
  call MPI_Recv(pf(1,1,1,-nimprove),nmat*nmat*nspin*nmargin,&
       &MPI_DOUBLE_COMPLEX,&
       &receive_rank,tag,MPI_COMM_WORLD,status,ierr)
  call MPI_Wait(ireq,status,ierr)
  !******
  if(myrank.ne.0)then
     send_rank=myrank-1
  else
     send_rank=nprocs-1
  end if
  if(myrank.ne.nprocs-1)then
     receive_rank=myrank+1
  else
     receive_rank=0
  end if
  tag=3
  call MPI_Isend(pf(1,1,1,1),nmat*nmat*nspin*nmargin,&
       &MPI_DOUBLE_COMPLEX,&
       &send_rank,tag,MPI_COMM_WORLD,ireq,ierr)
  call MPI_Recv(pf(1,1,1,nsite_local+1),nmat*nmat*nspin*nmargin,&
       &MPI_DOUBLE_COMPLEX,&
       &receive_rank,tag,MPI_COMM_WORLD,status,ierr) 
  call MPI_Wait(ireq,status,ierr)
  !***************************
  !*** boundary condition  ***
  !***************************
  if(nbc.EQ.0)then!pbc
     !don't do anything. 
  else if(nbc.EQ.1)then!apbc
     if(myrank.EQ.0)then
        do imat=1,nmat     
           do jmat=1,nmat
              do ispin=1,nspin
                 do isite=-(nmargin-1),0
                    pf(imat,jmat,ispin,isite)=&
                         &pf(imat,jmat,ispin,isite)*(-1d0,0d0)
                 end do
               end do
            enddo
        end do
     else if(myrank.EQ.nprocs-1)then
        do imat=1,nmat     
           do jmat=1,nmat
              do ispin=1,nspin
                 do isite=nsite_local+1,nsite_local+nmargin
                    pf(imat,jmat,ispin,isite)=&
                         &pf(imat,jmat,ispin,isite)*(-1d0,0d0)
                 end do
              end do
           end do
        end do
     end if
  end if

  return

END SUBROUTINE Adjust_margin_and_bc_pf
 
!***************************************************************
!This subroutine adjust the margin of chi (isite < 1 and isite > nsite_local), 
!by communicating with the neigbouring processes.
!It also properly sets the boundary condition (pbc or apbc).
SUBROUTINE Adjust_margin_and_bc_Chi(Chi,myrank,nprocs,nbc,nremez)

  implicit none
  include 'size_parallel.h'
  include 'mpif.h'
  !***** input *****
  integer nprocs,myrank,nbc,nremez
  !***** input & output *****
  double complex Chi(1:nremez,1:nmat,1:nmat,1:nspin,-(nmargin-1):nsite_local+nmargin)
  !***** for MPI *****
  integer send_rank,receive_rank,ireq,ierr,tag
  integer status(MPI_STATUS_SIZE)
  !**************************
  integer isite,imat,jmat,ispin,iremez

  !*************************
  !*** adjust the margin ***
  !************************* 
  if(myrank.ne.nprocs-1)then
     send_rank=myrank+1
  else
     send_rank=0
  end if
  if(myrank.ne.0)then
     receive_rank=myrank-1
  else
     receive_rank=nprocs-1
  end if
  tag=1
  call MPI_Isend(Chi(1,1,1,1,nsite_local-nimprove),&
       &nremez*nmat*nmat*nspin*(1+nimprove),&
       &MPI_DOUBLE_COMPLEX,&
       &send_rank,tag,MPI_COMM_WORLD,ireq,ierr)
  call MPI_Recv(Chi(1,1,1,1,-nimprove),&
       &nremez*nmat*nmat*nspin*(1+nimprove),&
       &MPI_DOUBLE_COMPLEX,&
       &receive_rank,tag,MPI_COMM_WORLD,status,ierr)
  call MPI_Wait(ireq,status,ierr)

  !******
  if(myrank.ne.0)then
     send_rank=myrank-1
  else
     send_rank=nprocs-1
  end if
  if(myrank.ne.nprocs-1)then
     receive_rank=myrank+1
  else
     receive_rank=0
  end if
  tag=3
  call MPI_Isend(Chi(1,1,1,1,1),nremez*nmat*nmat*nspin*(1+nimprove),&
       MPI_DOUBLE_COMPLEX,&
       send_rank,tag,MPI_COMM_WORLD,ireq,ierr)
  call MPI_Recv(Chi(1,1,1,1,nsite_local+1),nremez*nmat*nmat*nspin*(1+nimprove),&
       MPI_DOUBLE_COMPLEX,&
       receive_rank,tag,MPI_COMM_WORLD,status,ierr) 
  call MPI_Wait(ireq,status,ierr)
  !**************************
  !*** boundary condition *** 
  !**************************
  if(nbc.EQ.0)then!pbc
     !don't do anything. 
  else if(nbc.EQ.1)then!apbc
     if(myrank.EQ.0)then
        do ispin=1,nspin
           !$omp parallel do
           do jmat=1,nmat
              do imat=1,nmat  
                 do iremez=1,nremez          
                    do isite=-(nmargin-1),0
                       Chi(iremez,imat,jmat,ispin,isite)=&
                            Chi(iremez,imat,jmat,ispin,isite)*(-1d0,0d0)
                    end do
                 end do
              end do
           end do
        end do
     else if(myrank.EQ.nprocs-1)then
        do ispin=1,nspin
           !$omp parallel do
           do jmat=1,nmat
              do imat=1,nmat  
                 do iremez=1,nremez
                    do isite=nsite_local+1,nsite_local+nmargin
                       Chi(iremez,imat,jmat,ispin,isite)=&
                            Chi(iremez,imat,jmat,ispin,isite)*(-1d0,0d0)
                    end do
                 end do
              end do
           end do
        end do
     end if
  end if

  return

END SUBROUTINE Adjust_margin_and_bc_Chi
