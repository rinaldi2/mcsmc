!Derivative of (M*Chi)(t)_{ij} w.r.t. alpha_{k}  
SUBROUTINE Derivative_Dirac_alpha(nprocs,alpha,&
chi,Deriv_Mchi_alpha)

  implicit none
  include 'size_parallel.h'

  integer nprocs
  double precision alpha(1:nmat)
  double complex chi(1:nremez_md,1:nmat,1:nmat,1:nspin,-(nmargin-1):nsite_local+nmargin)
  double complex phase,phase2
  double precision aij
  double complex Deriv_Mchi_alpha(1:nremez_md,1:nmat,1:nmat,1:nmat,1:nspin,1:nsite_local)

  integer imat,jmat,kmat
  integer ispin
  integer isite
  integer iremez
  !***************************
  !***************************
  !***  kinetic part only  ***
  !***************************
  !***************************
  Deriv_Mchi_alpha=(0d0,0d0)
  !********************
  !*** Naive Action ***
  !********************
  if(nimprove.EQ.0)then
     do isite=1,nsite_local
        do imat=1,nmat
           do jmat=1,nmat
              
              aij=alpha(imat)-alpha(jmat)
              aij=aij/dble(nsite_local*nprocs)
              phase=dcmplx(dcos(aij))+(0d0,1d0)*dcmplx(dsin(aij))
              
              do iremez=1,nremez_md
                 do ispin=1,8
                    
                    kmat=jmat
                    Deriv_Mchi_alpha(iremez,kmat,imat,jmat,ispin+8,isite)=&
                         Deriv_Mchi_alpha(iremez,kmat,imat,jmat,ispin+8,isite)&
                         +chi(iremez,imat,jmat,ispin,isite+1)*phase&
                         /dcmplx(nsite_local*nprocs)*(0d0,-1d0)
                    
                    kmat=imat
                    Deriv_Mchi_alpha(iremez,kmat,imat,jmat,ispin+8,isite)=&
                         Deriv_Mchi_alpha(iremez,kmat,imat,jmat,ispin+8,isite)&
                         -chi(iremez,imat,jmat,ispin,isite+1)*phase&
                         /dcmplx(nsite_local*nprocs)*(0d0,-1d0)
                    
                    
                    kmat=jmat
                    Deriv_Mchi_alpha(iremez,kmat,imat,jmat,ispin,isite)=&
                         Deriv_Mchi_alpha(iremez,kmat,imat,jmat,ispin,isite)&
                         +chi(iremez,imat,jmat,ispin+8,isite-1)*dconjg(phase)&
                         /dcmplx(nsite_local*nprocs)*(0d0,-1d0)
                    
                    kmat=imat
                    Deriv_Mchi_alpha(iremez,kmat,imat,jmat,ispin,isite)=&
                         Deriv_Mchi_alpha(iremez,kmat,imat,jmat,ispin,isite)&
                         -chi(iremez,imat,jmat,ispin+8,isite-1)*dconjg(phase)&
                         /dcmplx(nsite_local*nprocs)*(0d0,-1d0)
                    
                 end do
              end do
           end do
        end do
     end do
     !***********************
     !*** Improved Action ***
     !***********************
  else if(nimprove.EQ.1)then
     do isite=1,nsite_local
        do imat=1,nmat
           do jmat=1,nmat
              
              aij=alpha(imat)-alpha(jmat)
              aij=aij/dble(nsite_local*nprocs)
              phase=dcmplx(dcos(aij))+(0d0,1d0)*dcmplx(dsin(aij))
              phase2=phase*phase
              do iremez=1,nremez_md
                 do ispin=1,8
                    
                    kmat=jmat
                    Deriv_Mchi_alpha(iremez,kmat,imat,jmat,ispin+8,isite)=&
                         Deriv_Mchi_alpha(iremez,kmat,imat,jmat,ispin+8,isite)&
                         +chi(iremez,imat,jmat,ispin,isite+1)*phase&
                         /dcmplx(nsite_local*nprocs)*(0d0,-2d0)&
                         +chi(iremez,imat,jmat,ispin,isite+2)*phase2&
                         /dcmplx(nsite_local*nprocs)*(0d0,1d0)  

                    kmat=imat
                    Deriv_Mchi_alpha(iremez,kmat,imat,jmat,ispin+8,isite)=&
                         Deriv_Mchi_alpha(iremez,kmat,imat,jmat,ispin+8,isite)&
                         -chi(iremez,imat,jmat,ispin,isite+1)*phase&
                         /dcmplx(nsite_local*nprocs)*(0d0,-2d0)&
                         -chi(iremez,imat,jmat,ispin,isite+2)*phase2&
                         /dcmplx(nsite_local*nprocs)*(0d0,1d0)
                    
                    
                    kmat=jmat
                    Deriv_Mchi_alpha(iremez,kmat,imat,jmat,ispin,isite)=&
                         Deriv_Mchi_alpha(iremez,kmat,imat,jmat,ispin,isite)&
                         +chi(iremez,imat,jmat,ispin+8,isite-1)*dconjg(phase)&
                         /dcmplx(nsite_local*nprocs)*(0d0,-2d0)&
                         +chi(iremez,imat,jmat,ispin+8,isite-2)*dconjg(phase2)&
                         /dcmplx(nsite_local*nprocs)*(0d0,1d0)

                    kmat=imat
                    Deriv_Mchi_alpha(iremez,kmat,imat,jmat,ispin,isite)=&
                         Deriv_Mchi_alpha(iremez,kmat,imat,jmat,ispin,isite)&
                         -chi(iremez,imat,jmat,ispin+8,isite-1)*dconjg(phase)&
                         /dcmplx(nsite_local*nprocs)*(0d0,-2d0)&
                         -chi(iremez,imat,jmat,ispin+8,isite-2)*dconjg(phase2)&
                         /dcmplx(nsite_local*nprocs)*(0d0,1d0)
                    
                 end do
              end do
           end do
        end do
     end do
  end if

  return
  
END SUBROUTINE Derivative_Dirac_alpha
