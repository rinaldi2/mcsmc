!*******************************************************************************
!*** Calculate the following quantities:                   *********************
!***           trx2bh = (1/N-1)*¥int dt Tr([X^{(BH)}]_I^2)/beta ****************
!***           offdiag = \sum |off-diag elements|^2/(N-1)/beta    ********
!******************************************************************
subroutine Calc_BH(xmat,trx2bh,offdiag,nprocs,myrank)

  implicit none

  include 'mpif.h'
  include 'size_parallel.h'

  integer nprocs,myrank
  double complex xmat(1:nmat,1:nmat,1:ndim,-(nmargin-1):nsite_local+nmargin)
  double complex xmat2(1:nmat,1:nmat,1:ndim,-(nmargin-1):nsite_local+nmargin)
  double precision trx2bh(1:ndim),trx2bh_local(1:ndim)
  double precision offdiag(1:ndim),offdiag_local(1:ndim),trace
  
  integer isite,idim
  integer imat,jmat
  integer IERR

  !Note that the trace part must be subtracted
  xmat2=xmat
  trx2bh_local=0d0
  do isite=1,nsite_local
     do idim=1,ndim
        trace=0d0
        do imat=1,nmat-1
           trace=trace+dble(xmat(imat,imat,idim,isite))
        end do
        trace=trace/dble(nmat-1)
        do imat=1,nmat-1
           xmat2(imat,imat,idim,isite)=xmat2(imat,imat,idim,isite)-dcmplx(trace)
        end do
        
        do jmat=1,nmat-1
           do imat=1,nmat-1
              trx2bh_local(idim)=trx2bh_local(idim)&
                   +dble(xmat2(imat,jmat,idim,isite)&
                   *dconjg(xmat2(imat,jmat,idim,isite)))
           end do
        end do
     end do
  end do
  
  call MPI_Reduce(trx2bh_local,trx2bh,ndim,MPI_DOUBLE_PRECISION,&
       MPI_SUM,0,MPI_COMM_WORLD,IERR)
  if(myrank.EQ.0)then
     trx2bh=trx2bh/dble((nmat-1)*nsite_local*nprocs)
  end if
  !The final result is stored at myrank=0.

  offdiag_local=0d0
  do isite=1,nsite_local
     do idim=1,ndim
        do imat=1,nmat-1
           offdiag_local(idim)=offdiag_local(idim)&
                +dble(xmat(imat,nmat,idim,isite)&
                *dconjg(xmat(imat,nmat,idim,isite)))
        end do
     end do
  end do
  

  call MPI_Reduce(offdiag_local,offdiag,ndim,MPI_DOUBLE_PRECISION,&
       MPI_SUM,0,MPI_COMM_WORLD,IERR)
  if(myrank.EQ.0)then
     offdiag=offdiag/dble((nmat-1)*nsite_local*nprocs)
  end if
  !The final result is stored at myrank=0.

  
  return

END subroutine Calc_BH
