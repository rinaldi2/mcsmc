! Calculate the largest eigenvalue of (D^dag*D),  
! by multiplying (D^dag*D) many times to a random vector. 
SUBROUTINE Largest_eigenvalue(nprocs,temperature,xmat,alpha,GAMMA10d,neig,largest_eig,myrank,nbc,nbmn,flux)

  implicit none

  include 'mpif.h'
  include 'size_parallel.h'
  !***** input *****
  integer neig,nbc,nbmn,nprocs,myrank
  double precision temperature,flux
  double complex xmat(1:nmat,1:nmat,1:ndim,-(nmargin-1):nsite_local+nmargin)
  double precision alpha(1:nmat)
  double complex GAMMA10d(1:ndim,1:nspin,1:nspin)
  !***** output *****
  double precision largest_eig
  !******************
  double complex phi1(1:nmat,1:nmat,1:nspin,-(nmargin-1):nsite_local+nmargin)
  double complex phi2(1:nmat,1:nmat,1:nspin,-(nmargin-1):nsite_local+nmargin)
  double complex trace
  double precision norm,norm_local
  integer imat,jmat
  integer ispin
  integer isite
  integer ieig,i
  double precision r1,r2
  !***** For MPI *****
  integer IERR
 
  !***********************************
  !**** generate a random vector. ****
  !***********************************
  if(myrank.GT.0)then
     !throw away some random numbers in order to 
     !synchronize with other nodes and avoid communication
     do i=1,nmat*nmat*nspin*nsite_local*myrank
       call BoxMuller(r1,r2)
     end do
  end if

  norm_local=0d0
  do imat=1,nmat     
     do jmat=1,nmat
        do ispin=1,nspin
           do isite=1,nsite_local
              call BoxMuller(r1,r2)
              phi1(imat,jmat,ispin,isite)=&
                   &(dcmplx(r1)+dcmplx(r2)*(0D0,1D0))/dcmplx(dsqrt(2d0))
              norm_local=norm_local+(r1*r1+r2*r2)*0.5d0
           end do
        end do
     end do
  end do
  !****************************
  !*** traceless projection ***
  !****************************
  do ispin=1,nspin
     do isite=1,nsite_local
        trace=(0d0,0d0)
        do imat=1,nmat
           trace=trace+phi1(imat,imat,ispin,isite)
        end do
        trace=trace/dcmplx(nmat)
        do imat=1,nmat
           norm_local=norm_local-dble(phi1(imat,imat,ispin,isite)*dconjg(phi1(imat,imat,ispin,isite)))
           phi1(imat,imat,ispin,isite)=phi1(imat,imat,ispin,isite)-trace
           norm_local=norm_local+dble(phi1(imat,imat,ispin,isite)*dconjg(phi1(imat,imat,ispin,isite)))
        end do
           
     end do
  end do

  if(myrank.LT.nprocs-1)then
     !throw away some random numbers in order to 
     !synchronize with other nodes and avoid communication
     do i=1,nmat*nmat*nspin*nsite_local*(nprocs-myrank-1)
        call BoxMuller(r1,r2)
     end do
  end if

  call MPI_Reduce(norm_local,norm,1,MPI_DOUBLE_PRECISION,&
       &MPI_SUM,0,MPI_COMM_WORLD,IERR)
  call MPI_Bcast(norm,1,MPI_DOUBLE_PRECISION,0,MPI_COMM_WORLD,IERR)
  
  norm=dsqrt(norm)
  norm=1d0/norm
  phi1=phi1*norm  
  !*********************************
  !*** adjust the margin and b.c.***
  !*********************************
  call Adjust_margin_and_bc_pf(phi1,myrank,nprocs,nbc)
  !******************************************
  !*** random vector has been generated.  ***
  !******************************************

  do ieig=1,neig
     !********************************************
     !*** phi1 -> phi2=D*phi -> phi1=D^dag*phi2***
     !********************************************
     call Multiply_Dirac(nprocs,temperature,&
          &xmat,alpha,phi1,phi2,GAMMA10d,nbmn,flux)
     !*********************************
     !*** adjust the margin and b.c.***
     !*********************************
     call Adjust_margin_and_bc_pf(phi2,myrank,nprocs,nbc)
     
     call Multiply_Dirac_dagger(nprocs,temperature,&
          &xmat,alpha,phi2,phi1,GAMMA10d,nbmn,flux)
     !*********************************
     !*** adjust the margin and b.c.***
     !*********************************
     call Adjust_margin_and_bc_pf(phi1,myrank,nprocs,nbc)
     !************************************x
     !*** calculate the norm of phi1.  ***
     !************************************
     norm_local=0d0
     do imat=1,nmat     
        do jmat=1,nmat
           do ispin=1,nspin
              do isite=1,nsite_local
                 norm_local=norm_local&
                      &+dble(dconjg(phi1(imat,jmat,ispin,isite)&
                      &*dconjg(phi1(imat,jmat,ispin,isite))))
              end do
           end do
        end do
     end do
     call MPI_Reduce(norm_local,norm,1,MPI_DOUBLE_PRECISION,&
          &MPI_SUM,0,MPI_COMM_WORLD,IERR)
     call MPI_Bcast(norm,1,MPI_DOUBLE_PRECISION,0,MPI_COMM_WORLD,IERR)
     norm=dsqrt(norm)
     norm=1d0/norm
     phi1=phi1*norm
     norm=1d0/norm
  end do
  
  largest_eig=norm

  return
  
END SUBROUTINE Largest_eigenvalue
