  !*************************************
  !*** Save the final configuration ****
  !*************************************


SUBROUTINE Save_Intermediate_Config(xmat,alpha,itraj)
  
  use mtmod !Mersenne twistor
  implicit none
  
  include 'size_parallel.h'
  include 'mpif.h'
  include 'unit_number.inc'
  !---------------------------------
  double precision alpha(1:nmat)
  double complex xmat(1:nmat,1:nmat,1:ndim,-(nmargin-1):nsite_local+nmargin)
  integer itraj
  !final configuration
  double complex, allocatable :: xmat_int(:,:,:,:)

  integer myrank,nprocs,IERR

  call MPI_COMM_SIZE(MPI_COMM_WORLD,NPROCS, IERR)
  call MPI_COMM_RANK(MPI_COMM_WORLD,MYRANK, IERR)

  if(myrank.eq.0)then
     allocate(xmat_int(1:nmat,1:nmat,1:ndim,1:nsite_local*nprocs))
  end if
  !Gather xmat from all MPI processes to myrank=0
  call MPI_Gather(xmat(1,1,1,1),nmat*nmat*ndim*nsite_local,&
       &MPI_DOUBLE_COMPLEX,&
       &xmat_int(1,1,1,1),nmat*nmat*ndim*nsite_local,MPI_DOUBLE_COMPLEX,&
       &0,MPI_COMM_WORLD,IERR)

  if(myrank.eq.0)then
     call mtsaveu(unit_intermediate_config)  
     write(unit_intermediate_config,*) itraj
     write(unit_intermediate_config,*) xmat_int
     write(unit_intermediate_config,*) alpha
     deallocate(xmat_int)
  end if

  return

END SUBROUTINE Save_Intermediate_Config
