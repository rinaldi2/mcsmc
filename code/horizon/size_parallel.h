!----------------------------------------
!     Number of sites along t-direction
integer nsite_local
parameter(nsite_local=4)
!----------------------------------------
!     Size of matrices
integer nmat
parameter(nmat=6)
!----------------------------------------
!     number of remez coefficients
integer nremez_md,nremez_pf
parameter(nremez_md=15)
parameter(nremez_pf=15)
!--------------------------------------
integer ndim ! fix to 9
parameter(ndim=9)
integer nspin ! fix to 16
parameter(nspin=16)
!----------------------------------------
!   nimprove=0 -> no improvement
!   nimprove=1 -> O(a^2) improvement. Note that communication cost increases.
integer nimprove
parameter(nimprove=1)
integer nmargin!size of the margin
parameter(nmargin=nimprove+1)
