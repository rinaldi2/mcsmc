SUBROUTINE measurements(xmat,alpha,nbc,nbmn,myrank,nprocs,temperature,flux,&
     &GAMMA10d,neig_max,neig_min,ham_init,ham_fin,itraj,ntrial,iteration,&
     &max_err,max_iteration,ncv,n_bad_CG,nacceptance)

  implicit none
  include 'size_parallel.h'
  include 'unit_number.inc'
  include 'mpif.h'
  !input
  double complex xmat(1:nmat,1:nmat,1:ndim,-(nmargin-1):nsite_local+nmargin)
  double precision alpha(1:nmat)
  integer nbc,nbmn,myrank,nprocs,ntrial,iteration,itraj,nacceptance
  integer ncv,n_bad_CG
  integer neig_max,neig_min
  double precision temperature,flux
  double complex Gamma10d(1:ndim,1:nspin,1:nspin)
  double precision max_err
  integer max_iteration

  double precision trx2,com2,Pol,largest_eig,smallest_eig,energy,&
       &ham_fin,ham_init,distance_local(1:ndim),distance(1:ndim),&
       &trx(1:ndim,1:nsite_local),trx2bh(1:ndim),offdiag(1:ndim)
  integer idim,isite,ierr

  

  !measurements. 
  call Calc_TrX2(xmat,trx2,nprocs,myrank)
  call Calc_Com2(xmat,com2,nprocs,myrank)
  call Calc_energy(temperature,xmat,alpha,&
       &energy,nprocs,myrank,nbmn,flux)
  call Calc_BH(xmat,trx2bh,offdiag,nprocs,myrank)
  if(myrank.EQ.0)then
     call Calc_Polyakov(alpha,Pol)
  end if
  !largest eigenvalue of D^dag*D. 
  if(neig_max.GT.0)then
     call Largest_eigenvalue(nprocs,temperature,&
          &xmat,alpha,GAMMA10d,neig_max,largest_eig,myrank,nbc,nbmn,flux)
  end if
  !smallest eigenvalue of D^dag*D. 
  if(neig_min.GT.0)then
     call Smallest_eigenvalue(nprocs,temperature,&
          &xmat,alpha,GAMMA10d,neig_min,smallest_eig,myrank,nbc,&
          &max_err,max_iteration,nbmn,flux)
  end if

  call Calc_TrX(xmat,trx,nprocs,myrank)
  distance_local=0d0
  do idim=1,ndim
     do isite=1,nsite_local
        distance_local(idim)=distance_local(idim)+&
        &(trx(idim,isite)-xmat(nmat,nmat,idim,isite))/dble(nmat-1)&
             &-xmat(nmat,nmat,idim,isite)
     end do
  end do
  call MPI_Reduce(distance_local,distance,ndim,MPI_DOUBLE_PRECISION,&
       &MPI_SUM,0,MPI_COMM_WORLD,IERR)
  distance=distance/dble(nprocs*nsite_local)
  !**************
  !*** output ***
  !**************
  if(myrank.EQ.0)then
     if((neig_max.EQ.0).AND.(neig_min.EQ.0))then
        
        write(unit_measurement,'(I8,1x,f15.9,1x,I4,1x,I8,1x,I8,5(1x,f15.7))')&
             &itraj,ham_fin-ham_init,ncv,n_bad_CG,iteration,energy,&
             &Pol,trx2,com2,dble(nacceptance)/dble(ntrial)
        
        write(*,'(I8,1x,f15.9,1x,I4,1x,I8,1x,I8,5(1x,f15.7))')&
             &itraj,ham_fin-ham_init,ncv,n_bad_CG,iteration,energy,&
             &Pol,trx2,com2,dble(nacceptance)/dble(ntrial)
        
     else  if((neig_max.GT.0).AND.(neig_min.EQ.0))then
        
        write(unit_measurement,'(I8,1x,f15.9,1x,I4,1x,I8,1x,I8,8(1x,f15.7))')&
             &itraj,ham_fin-ham_init,ncv,n_bad_CG,iteration,energy,&
             &Pol,trx2,com2,distance(1),distance(2),largest_eig,&
             &dble(nacceptance)/dble(ntrial)
        write(*,'(I8,1x,f15.9,1x,I4,1x,I8,1x,I8,8(1x,f15.7))')&
             &itraj,ham_fin-ham_init,ncv,n_bad_CG,iteration,energy,&
             &Pol,trx2,com2,distance(1),distance(2),largest_eig,&
             &dble(nacceptance)/dble(ntrial)
        
     else  if((neig_max.EQ.0).AND.(neig_min.GT.0))then
        
        write(unit_measurement,'(I8,1x,f15.9,1x,I4,1x,I8,1x,I8,8(1x,f15.7))')&
             &itraj,ham_fin-ham_init,ncv,n_bad_CG,iteration,energy,&
             &Pol,distance(1),distance(2),trx2,com2,smallest_eig,&
             &dble(nacceptance)/dble(ntrial)
        write(*,'(I8,1x,f15.9,1x,I4,1x,I8,1x,I8,8(1x,f15.7))')&
             &itraj,ham_fin-ham_init,ncv,n_bad_CG,iteration,energy,&
             &Pol,trx2,com2,distance(1),distance(2),smallest_eig,&
             &dble(nacceptance)/dble(ntrial)
        
     else  if((neig_max.GT.0).AND.(neig_min.GT.0))then
        write(unit_measurement,'(I8,1x,f15.9,1x,I4,1x,I8,1x,I8,9(1x,f15.7))')&
             &itraj,ham_fin-ham_init,ncv,n_bad_CG,iteration,energy,&
             &abs(Pol),trx2,com2,distance(1),distance(2),largest_eig,smallest_eig,&
             &dble(nacceptance)/dble(ntrial)
        write(*,'(I8,1x,f15.9,1x,I4,1x,I8,1x,I8,9(1x,f15.7))')&
             &itraj,ham_fin-ham_init,ncv,n_bad_CG,iteration,energy,&
             &abs(Pol),trx2,com2,distance(1),distance(2),largest_eig,smallest_eig,&
             &dble(nacceptance)/dble(ntrial)
     end if

     write(unit_trxbh,'(I8,1x,18(1x,f15.7))')&
          &itraj,trx2bh(1),trx2bh(2),trx2bh(3),trx2bh(4),trx2bh(5),&
          &trx2bh(6),trx2bh(7),trx2bh(8),trx2bh(9),&
          &offdiag(1),offdiag(2),offdiag(3),offdiag(4),offdiag(5),&
          &offdiag(6),offdiag(7),offdiag(8),offdiag(9)
     write(*,'(I8,1x,18(1x,f15.7))')&
          &itraj,trx2bh(1),trx2bh(2),trx2bh(3),trx2bh(4),trx2bh(5),&
          &trx2bh(6),trx2bh(7),trx2bh(8),trx2bh(9),&
          &offdiag(1),offdiag(2),offdiag(3),offdiag(4),offdiag(5),&
          &offdiag(6),offdiag(7),offdiag(8),offdiag(9)
     
  end if

  return 

END SUBROUTINE measurements
