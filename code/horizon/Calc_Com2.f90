!*******************************************************
!*** Calculate com2 = (-1/N)*¥int dt Tr([X_I,X_J]^2) ***
!*******************************************************
subroutine Calc_Com2(xmat,com2,nprocs,myrank)

  implicit none

  include 'mpif.h'
  include 'size_parallel.h'
  !********** input ********** 
  integer nprocs,myrank
  double complex xmat(1:nmat,1:nmat,1:ndim,-(nmargin-1):nsite_local+nmargin)
  !********** outout **********
  double precision com2
  !****************************
  double precision com2_local
  double complex com(1:nmat,1:nmat)
  integer isite,idim,jdim
  integer imat,jmat,kmat
  integer IERR

  com2_local=0d0
  do isite=1,nsite_local
     do idim=1,ndim-1
        do jdim=idim+1,ndim
           com=(0d0,0d0)
           do imat=1,nmat
              do jmat=1,nmat
                 do kmat=1,nmat
                    com(imat,jmat)=com(imat,jmat)+&
                         &xmat(imat,kmat,idim,isite)*xmat(kmat,jmat,jdim,isite)&
                         &-xmat(imat,kmat,jdim,isite)*xmat(kmat,jmat,idim,isite)
                 end do
              end do
           end do
           do jmat=1,nmat
              do imat=1,nmat
                 com2_local=com2_local+&
                      &dble(com(imat,jmat)*dconjg(com(imat,jmat)))
              end do
           end do
        end do
     end do
  end do

  call MPI_Reduce(com2_local,com2,1,MPI_DOUBLE_PRECISION,&
       &MPI_SUM,0,MPI_COMM_WORLD,IERR)
  if(myrank.EQ.0)then
     com2=com2/dble(nmat*nsite_local*nprocs)*2d0
  end if
  
  !The final result is stored at myrank=0.

  return

END subroutine Calc_Com2
