! Generate P_xmat with Gaussian weight.
! We do not have to care about the traceless condition. 
SUBROUTINE Generate_P_xmat(P_xmat,nprocs,myrank)
  
  implicit none
  include 'size_parallel.h'
  integer nprocs,myrank
  integer imat,jmat,idim,isite,irank
  double precision r1,r2

  double complex P_xmat(1:nmat,1:nmat,1:ndim,1:nsite_local)
  
  if(myrank.GT.0)then
     !throw away some random numbers in order to 
     !synchronize with other nodes and avoid communication
     do irank=1,myrank
        do isite=1,nsite_local
           do idim=1,ndim
              do imat=1,nmat-1
                 do jmat=imat+1,nmat
                    call BoxMuller(r1,r2)
                 end do
              end do
              do imat=1,nmat
                 call BoxMuller(r1,r2)
              end do
           end do
        end do
     end do
  end if
    
  do isite=1,nsite_local
     do idim=1,ndim
        do imat=1,nmat-1
           do jmat=imat+1,nmat
              call BoxMuller(r1,r2)
              P_xmat(imat,jmat,idim,isite)=&
                   dcmplx(r1/dsqrt(2d0))+dcmplx(r2/dsqrt(2d0))*(0D0,1D0)
              P_xmat(jmat,imat,idim,isite)=&
                   dcmplx(r1/dsqrt(2d0))-dcmplx(r2/dsqrt(2d0))*(0D0,1D0)
           end do
        end do
        do imat=1,nmat
           call BoxMuller(r1,r2)
           P_xmat(imat,imat,idim,isite)=dcmplx(r1)
        end do
     end do
  end do
  
  if(myrank.LT.nprocs-1)then
     !throw away some random numbers in order to 
     !synchronize with other nodes and avoid communication
     do irank=1,nprocs-myrank-1
        do isite=1,nsite_local
           do idim=1,ndim
              do imat=1,nmat-1
                 do jmat=imat+1,nmat
                    call BoxMuller(r1,r2)
                 end do
              end do
              do imat=1,nmat
                 call BoxMuller(r1,r2)
              
              end do
           end do
        end do
     end do
  end if

  return
  
END SUBROUTINE Generate_P_xmat
