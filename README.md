# README #

This repository contains the parallel and serial versions of the code used to run lattice simulations of the BFSS model

### What is this repository for? ###

* Keep an organized repository for different versions of the code
* Code development for different platforms
* [Based on this code](https://sites.google.com/site/hanadamasanori/home/mmmm)

### How do I get set up? ###

* It should be straightforward to compile this code with Intel of GNU compilers
* All files should be included, but maybe we want to add a dependency on LAPACK
* At some point use [Grid](https://github.com/paboyle/Grid) as a base library
* We should come up with some simple tests to check the code behavior

### Contribution guidelines ###

* There should be different branches based on the application
* Probably moving from Fortran to C++

### Who do I talk to? ###

* Enrico Rinaldi (Repo owner)
* Masanori Hanada
* Evan Berkowitz