#!/bin/bash
source ~/.bash_colors
echo -e "${BBlue} ======================================== FITTING ====================================== ${Color_Off}"
# select cut and statistics
if [[ $# -ne 2 ]] ; then
    echo "Usage: `basename $0` <CUT> <SUFF>" && exit
fi
cut=$1
suff=$2
# create the summary data files for 2d plotting
(cd data_avg && ./merge_3D_datafiles.sh $cut $suff)
# loop over all files that have more than 9 points
rm -f listfilestofit2d.dat
for file in `qls data_avg/t[0-9]*_${cut}_${suff}.dat` ; do
    if [[ $(wc -l ${file} | ak 1) -gt 9 ]] ; then
	echo $file >> listfilestofit2d.dat
    fi
done
[ ! -f listfilestofit2d.dat ] && echo "no files" && exit 1
echo "There are $(wc -l listfilestofit2d.dat | ak 1) files that require fitting"
# loop over files
for file in `cat listfilestofit2d.dat` ; do
    echo -e "${Red}--- $file ---${Color_Off}"
    T=$(echo $file | cut -d'_' -f2 | sed -e"s|.*t||")
    TT=$(echo $T | sed -e"s|\(.\)|\1.|")
    obs=$(echo $file | cut -d'_' -f3)
    action=$(echo $file | cut -d'_' -f4)
    echo $N $T $TT $obs $action $cut
    sed -e"s|XTTX|${T}|; \
	   s|XTTTX|${TT}|; \
	   s|XOOX|${obs}|; \
	   s|XAAX|${action}|; \
	   s|XCCX|${cut}|;\
	   s|XSSX|${suff}|" continuum_2d.set.template > continuum_2d.set
    ./splot_continuum_limit.gp
    mv fit_continuum_2d_f.log fits/fit_continuum_2d_f_t${T}_${obs}_${action}_${cut}_${suff}.log
    mv fit_continuum_2d_g.log fits/fit_continuum_2d_g_t${T}_${obs}_${action}_${cut}_${suff}.log
done
