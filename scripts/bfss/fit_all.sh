#!/bin/bash
source ~/.bash_colors
echo -e "${BBlue} ======================================== FITTING ====================================== ${Color_Off}"
# select cut and statistics
if [[ $# -ne 2 ]] ; then
    echo "Usage: `basename $0` <CUT> <SUFF>" && exit
fi
cut=$1
suff=$2
# loop over all files in data_avg that have more than 3 points
rm -f listfilestofit.dat
for file in `qls data_avg/n[0-9]*t[0-9]*_${cut}_${suff}.dat` ; do
    if [[ $(wc -l ${file} | ak 1) -gt 4 ]] ; then
        if [[ $(tail -n2 ${file} | head -n1 | ak 1) -gt 23 ]] ; then
	    echo $file >> listfilestofit.dat
        fi
    fi
done
[ ! -f listfilestofit.dat ] && echo "no files" && exit 1
echo "There are $(wc -l listfilestofit.dat | ak 1) files that require fitting"
# loop over files
for file in `cat listfilestofit.dat` ; do
    echo -e "${Red}--- $file ---${Color_Off}"
    N=$(echo $file | sed -e"s|.*/n||;s|t.*||")
    T=$(echo $file | cut -d'_' -f2 | sed -e"s|.*t||")
    TT=$(echo $T | sed -e"s|\(.\)|\1.|")
    obs=$(echo $file | cut -d'_' -f3)
    action=$(echo $file | cut -d'_' -f4)
    echo $N $T $TT $obs $action $cut
    sed -e"s|XNNX|${N}|; \
    	   s|XTTX|${T}|; \
	   s|XTTTX|${TT}|; \
	   s|XOOX|${obs}|; \
	   s|XAAX|${action}|; \
	   s|XCCX|${cut}|; \
	   s|XSSX|${suff}|" continuum.set.template > continuum.set
    ./plot_continuum_limit.gp
    mv fit_continuum_linear.log fits/fit_continuum_linear_n${N}t${T}_${obs}_${action}_${cut}_${suff}.log
    mv fit_continuum_quadratic.log fits/fit_continuum_quadratic_n${N}t${T}_${obs}_${action}_${cut}_${suff}.log
done
