#!/bin/bash
# This script merges the available streams for a specific set of parameters (N,L,T,action) with a fixed cut on the statistics of each stream.
# It will also produce a summary of the statistical properties (avg,err,autocorrtime, bins) of each stream for a quick comparison by eye
# A stream can be excluded from the final join operation if it's properties are not satisfactory (e.g. instability, not enough thermalization, etc...)
##########
# Check the properties of each stream individually
#########
#obs=trx
#for T in 05 06 07 ; do
#    file=merge_streams_T${T}_${obs}.log
#    rm -rf $file
#    for stream in a b c d ; do
#        echo ----------------------------- $stream
#        if [[ "$stream" = "a" ]] ; then
#            cut=1000
#        else
#            cut=100
#        fi
#        basic_analysis -n N24S24T${T}${stream}_improved/${obs}.dat -c 2 -o 2 -i ${cut} > tmpbuff
#        echo -n $stream $(grep -e"Obs->avr" tmpbuff | ak 2,3) >> $file
#        echo " # $(grep -e"Total points" tmpbuff | ak 4) | $(grep -e"Obs->numbins" tmpbuff | ak 2)" >> $file
#    done
#    echo ---------------------- $T
#done
##########
# Append the streams {b,c,d} to {a}
##########
streams="b c d"
param=N24S24
for obs in energy pol trx trxy ; do
    echo "********* appending streams for ${obs} **********"
    for T in 05 06 07 ; do
	      echo "--- T=${T}"
	      if [[ ! -d ${param}T${T}_improved ]] ; then
          echo "ERROR: ${param}T${T}_improved is not a directory. Where do I merge??"
          exit 1
        fi
        #ls -l ${param}T${T}_improved/${obs}.dat
        # make a copy of the original folder calling it stream `a`
        cp -r ${param}T${T}_improved ${param}T${T}a_improved
        # merge into the original folder which is the one used for the analysis
        for stream in $streams ; do
          echo "--------- stream=${stream}"
          awk 'NR>100' ${param}T${T}${stream}_improved/${obs}.dat >> ${param}T${T}_improved/${obs}.dat
        done
    done
done
