#!/bin/bash
# this is for the improved sets
rm -f complete_dataset.dat
for a in n*t*_energy_improved_1000_all.dat ; do
    n=$(echo $a | sed -e"s|t.*||;s|n||")
    t=$(echo $a | sed -e"s|n..t||;s|_.*||;s|\(.\)|\1.|")
    echo $n $t $a
    awk -v n=$n -v t=$t '$1~/[0-9]/ {print n,t,$1,$2,$3}' $a
    awk -v n=$n -v t=$t '$1~/[0-9]/ {print n,t,$1,$2,$3}' $a >> complete_dataset.dat
    echo -----------------
done
# this is for the unimproved sets
rm -f complete_dataset_u.dat
for a in n*t*_energy_unimproved_1000_all.dat ; do
    n=$(echo $a | sed -e"s|t.*||;s|n||")
    t=$(echo $a | sed -e"s|n..t||;s|_.*||;s|\(.\)|\1.|")
    echo $n $t $a
    awk -v n=$n -v t=$t '$1~/[0-9]/ {print n,t,$1,$2,$3}' $a
    awk -v n=$n -v t=$t '$1~/[0-9]/ {print n,t,$1,$2,$3}' $a >> complete_dataset_u.dat
    echo -----------------
done
