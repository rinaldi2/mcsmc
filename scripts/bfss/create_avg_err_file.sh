#!/bin/bash
source ~/.bash_colors
# do analysis only if min_traj measurements are present
min_traj=1000
# fix binsize to BINSIZE
# BINSIZE=100
# usage:
if [[ $# -ne 6 ]] ; then
    echo "Usage: `basename $0` <N> <T> <CUT> <TOT> <OBS> <ACTION>" && exit
fi
# set parameters from std input
N=$1
T=$2
cut=$3
suff=$4
obs=$5
act=$6
file="n${N}t${T}_${obs}_${act}_${cut}_${suff}.dat"
datadir=$(pwd)/../data_from_oslic
sizes=$(qls -d ${datadir}/N${N}S*T${T}_${act} | sed -e"s|.*S||;s|T.*||" | sort -n)
if [[ -z "${sizes}" ]] ; then
    echo  -e "${Red}--- ${datadir}/N${N}S*T${T}_${act} is not a directory --- ${Color_Off}"
    exit 1
fi
echo "------------------------------------------------------------------------------------"
echo -n -e "${BBlue} Creating file $file using the average value for the ${obs} for L= "
echo -e $sizes ${Color_Off}
[[ ! "${suff}" == "all" ]] && echo -e "${BRed} fixing statistics! starting=${cut} ending=${suff} ${Color_Off}"
echo "# with ${cut} trajectories discarded for thermalization # included | bins" > $file
for a in $sizes ; do
    FILE="${datadir}/N${N}S${a}T${T}_${act}/${obs}.dat"
    total=$(wc -l $FILE | ak 1)
    used=$[total-cut]
    if [[ "$used" -gt "${min_traj}" ]] ; then
	   if [[ -f $FILE ]] ; then
	    if [[ "${suff}" == "all" ]] ; then
		      basic_analysis -n $FILE -c 2 -o 2 -i ${cut} > tmpbuff
	    else
		      basic_analysis -n $FILE -c 2 -o 2 -i ${cut} -f ${suff} > tmpbuff
	    fi
	    if grep Warning tmpbuff ; then
		      echo -e "${Green} ${datadir}/N${N}S${a}T${T}_${act}/${obs}.dat warning --- ${Color_Off}"
	    fi
	    echo -n $a $(grep -e"Obs->avr" tmpbuff | ak 2,3) >> $file
	    echo " # $(grep -e"Total points" tmpbuff | ak 4) | $(grep -e"Obs->numbins" tmpbuff | ak 2)" >> $file
	   fi
    else
	     echo "L=$a does not have at least ${min_traj} configurations ... "
    fi
done
