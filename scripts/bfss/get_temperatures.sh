#!/bin/bash
rm -f listoftemps.txt
for i in `cat ../data_from_oslic/listsummaryfiles.txt` ; do
    t=$(echo ${i} | cut -d'_' -f1 | sed -e"s|.*T||")
    tt=$(echo $t  | sed -e"s|\(.\)|\1.|")
    echo $tt $t >> listoftemps.txt
#    echo temperature $t
done
cat listoftemps.txt | sort -n | uniq
