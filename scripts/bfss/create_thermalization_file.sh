#!/bin/bash
source ~/.bash_colors
if [[ $# -ne 2 ]] ; then
    echo "Usage: `basename $0` <filepath> <output>" && exit
fi
FILE=$1
WORD=$2
START=0
FINAL=1100
STEP=50
BINWIDTH=50
if [[ ! -f $FILE ]] ; then
    echo "File $FILE not found!" && exit
fi
echo -e "${Red} -----------------------> ${WORD}_stability.dat ${Color_Off}"
total=$(wc -l $FILE | ak 1)
echo "# Last trajs Ave JK-Err (total=${total})"  > ${WORD}_stability.dat
for ((last=${START}; last<${FINAL}; last+=${STEP})) ; do
    echo $last $(basic_analysis -n $FILE -c 2 -o 2 -b ${BINWIDTH} -i -${last} | grep -e"Obs->avr" | ak 2,3) >> ${WORD}_stability.dat
done
echo -e "${Red} -----------------------> ${WORD}_thermalization.dat ${Color_Off}"
echo "# First trajs Ave JK-Err (total=${total})" > ${WORD}_thermalization.dat
for ((cut=${START}; cut<${FINAL}; cut+=${STEP})) ; do
#    echo $cut $(basic_analysis -n $FILE -c 2 -o 2 -b ${BINWIDTH} -i ${cut} -f ${FINAL} | grep -e"Obs->avr" | ak 2,3)  >> ${WORD}_thermalization.dat
    echo $cut $(basic_analysis -n $FILE -c 2 -o 2 -b ${BINWIDTH} -i ${cut} | grep -e"Obs->avr" | ak 2,3)  >> ${WORD}_thermalization.dat
done
echo -e "${Red} -----------------------> ${WORD}_binning.dat ${Color_Off}"
last=10000
#last=${FINAL}
echo "# BINSIZE Ave JK-Err (total=${last})" > ${WORD}_binning.dat
for ((binsize=2; binsize<152; binsize+=2)) ; do
    echo -n $binsize $(basic_analysis -n $FILE -c 2 -o 2 -b $binsize -i -${last} | grep -e"Obs->avr" | ak 2,3)  >> ${WORD}_binning.dat
    echo " # $last" >> ${WORD}_binning.dat
done
echo -e "${Red} -----------------------> ${WORD}_sliding.dat ${Color_Off}"
sliding=1000
echo "# Start trajs Ave JK-Err (total=${sliding})" > ${WORD}_sliding.dat
for ((cut=1; cut<$[FINAL-sliding]; cut+=${STEP})) ; do
    echo $cut $(basic_analysis -n $FILE -c 2 -o 2 -b ${BINWIDTH} -i $cut -f $[cut+sliding] | grep -e"Obs->avr" | ak 2,3)  >> ${WORD}_sliding.dat
done
