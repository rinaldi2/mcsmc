#!/bin/bash
###############################################################################
# this script creates the summary_fit.tex file and compiles it to make summary_fit.pdf
###############################################################################
# select cut and statistics
if [[ $# -ne 2 ]] ; then
    echo "Usage: `basename $0` <CUT> <LAST>" && exit
fi
cut=$1
last=$2
# everything is in this directory
maindir="/Users/rinaldi2/Documents/Projects/Strings"
# notes are in this directory
notesdir="notes"
# fit plots are in this directory
filesdir="plots"
# suffix for data files
suff="energy_improved_${cut}_${last}"
# name of final tex and pdf file
name="summary_fit_${cut}_${last}"
# create list of fit files to plot for the following temperatures:
templist="05 06 065 07 075 08 085 09 10"
listnames="listsummplots.txt"
rm -f ${listnames}
for t in $templist ; do
    rm -f listfitplots.txt
    for file in `qls ${maindir}/${filesdir}/n*t${t}_${suff}.png` ; do
	echo $file >> listfitplots.txt
    done
    # combine all fit plots for a given temperature at various N in nice PDF bundles using montage
    montage -density 300 -tile 2x0 -geometry +5+5 -border 2 $(cat listfitplots.txt) ${maindir}/${filesdir}/summary_t${t}_${suff}.pdf
    echo  ${maindir}/${filesdir}/summary_t${t}_${suff}.pdf >> ${listnames}
    # combine 3d plots for a given temperature with n=24 plots as a reference
    if [ -f ${maindir}/${filesdir}/n24t${t}_${suff}.png ] && [ -f ${maindir}/${filesdir}/t${t}_${suff}.png ]; then
	montage -density 300 -tile 2x0 -geometry +5+5 -border 2 ${maindir}/${filesdir}/n24t${t}_${suff}.png ${maindir}/${filesdir}/t${t}_${suff}.png ${maindir}/${filesdir}/summary_t${t}_2d_${suff}.pdf
	echo ${maindir}/${filesdir}/summary_t${t}_2d_${suff}.pdf >> ${listnames}
    else
	montage -density 300 -tile 2x0 -geometry +5+5 -border 2 ${maindir}/${filesdir}/n16t${t}_${suff}.png ${maindir}/${filesdir}/t${t}_${suff}.png ${maindir}/${filesdir}/summary_t${t}_2d_${suff}.pdf
	echo ${maindir}/${filesdir}/summary_t${t}_2d_${suff}.pdf >> ${listnames}
    fi
done
# count the summary PDF files
echo "There are $(wc -l ${listnames} | ak 1) summary plots for fits."
# start the summary.tex file with the header
cat header_fit.tex > ${name}.tex
# modify the figure template for each guy in the list
for file in `cat ${listnames}` ; do
#    N=$(echo $file | sed -e"s|S.*||;s|N||")
#    L=$(echo $file | sed -e"s|.*S||;s|T.*||")
#    stream=$(echo $file | cut -d'_' -f1 | sed -e"s|.*[0-9]||")
#    if [[ -z "${stream}" ]] ; then stream="a" ; fi
    T=$(echo $file | cut -d'_' -f2 | sed -e"s|t||")
    TT=$(echo $T | sed -e"s|\(.\)|\1.|;s|[a-z]||")
    label=$(echo $file | cut -d'_' -f3)
    if [[ "${label}" == "energy" ]] ; then label="at fixed N" ; fi
    figurename=${file##*/}
    echo $T $TT
    sed -e"s|XFFX|${file}|;\
    	   s|XCCX|fits at T=${TT} $label|;\
	   s|XLLX|${figurename%%.pdf}|" figure_template.tex.tmp > figure_${figurename%%.pdf}_fit.tex
# input every figure into the summary.tex
    echo "\input{figure_${figurename%%.pdf}_fit.tex}" >> ${name}.tex
done
# finish the tex document
cat tail.tex >> ${name}.tex
# compile the document
pdflatex ${name}.tex
pdflatex ${name}.tex
# open the summary PDF file
open ${name}.pdf
