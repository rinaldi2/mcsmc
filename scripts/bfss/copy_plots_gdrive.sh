#!/bin/bash
# syncing measurement files and plots to Google Drive
source ~/.bash_colors
echo -e "${BBlue} ======================================== SYNC PLOTS TO GDRIVE ====================================== ${Color_Off}"
cd data_from_oslic
for dir in N*S??T* ; do
    echo -e "${BRed} sync $dir ${Color_Off}"
    mkdir -p ~/Google\ Drive/LLNL/BlackStrings/${dir}
    cp -p ${dir}/*.png ~/Google\ Drive/LLNL/BlackStrings/${dir}/.
done
cd ../
echo -e "${BRed} sync plots ${Color_Off}"
cp data_avg/*.png plots/*.png data_thermalization/*.png *.png ~/Google\ Drive/LLNL/BlackStrings/.
echo -e "${BRed} sync fits ${Color_Off}"
mkdir -p ~/Google\ Drive/LLNL/BlackStrings/fits
cp -p fits/*.log ~/Google\ Drive/LLNL/BlackStrings/fits/.
echo -e "${BRed} sync data averages ${Color_Off}"
mkdir -p ~/Google\ Drive/LLNL/BlackStrings/data
cp -p data_avg/*.dat ~/Google\ Drive/LLNL/BlackStrings/data/.
echo -e "${BBlue} done ${Color_Off}"
