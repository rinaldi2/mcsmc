#!/bin/bash
source ~/.bash_colors
echo -e "${BBlue} ======================================== CREATE THERMALIZATION PLOTS ====================================== ${Color_Off}"
datadir=$(pwd)/../data_from_oslic
echo $(ls -d ${datadir}/N*S*T*_*) > listdirs.txt
for obs in energy pol trx ; do
    for filein in `cat listdirs.txt` ; do
	fileout=$(echo ${filein##*oslic/}  | tr '[:upper:]' '[:lower:]')
	./create_thermalization_file.sh ${filein}/${obs}.dat ${fileout}_${obs}
    done
    echo -e "${BBlue} ------ done ${obs} ------ ${Color_Off}"
done
