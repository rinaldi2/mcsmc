#!/usr/local/bin/gnuplot
set term x11 persist
set multiplot layout 3, 2 columnsfirst font "Arial,9"
set title "Thermalization study T=0.7 unimproved"
set xl 'Number of CUT trajectories'
set yl 'Energy (average and JK errors)'
plot 'n16s32t07_unimproved_energy_thermalization.dat' u 1:2:3 w yerr t 'L=32'
set title "Stability study T=0.7 unimproved"
set xl 'Number of LAST trajectories'
set yl 'Energy (average and JK errors)'
plot 'n16s32t07_unimproved_energy_stability.dat' u 1:2:3 w yerr t 'L=32'
set title "Binning study T=0.7 unimproved"
set xl 'Number of measures in one BIN'
set yl 'JK Error (average)'
plot 'n16s32t07_unimproved_energy_binning.dat' u 1:3 w l t 'L=32'
set title "Thermalization study T=0.7 improved"
set xl 'Number of CUT trajectories'
set yl 'Energy (average and JK errors)'
plot 'n16s8t07_improved_energy_thermalization.dat' u 1:2:3 w yerr t 'L=8',\
     'n16s12t07_improved_energy_thermalization.dat' u 1:2:3 w yerr t 'L=12',\
     'n16s16t07_improved_energy_thermalization.dat' u 1:2:3 w yerr t 'L=16',\
     'n16s24t07_improved_energy_thermalization.dat' u 1:2:3 w yerr t 'L=24',\
     'n16s32t07_improved_energy_thermalization.dat' u 1:2:3 w yerr t 'L=32'
set title "Stability study T=0.7 improved"
set xl 'Number of LAST trajectories'
set yl 'Energy (average and JK errors)'
plot 'n16s8t07_improved_energy_stability.dat' u 1:2:3 w yerr t 'L=8',\
     'n16s12t07_improved_energy_stability.dat' u 1:2:3 w yerr t 'L=12',\
     'n16s16t07_improved_energy_stability.dat' u 1:2:3 w yerr t 'L=16',\
     'n16s24t07_improved_energy_stability.dat' u 1:2:3 w yerr t 'L=24',\
     'n16s32t07_improved_energy_stability.dat' u 1:2:3 w yerr t 'L=32'
set title "Binning study T=0.7 improved"
set xl 'Number of measures in one BIN'
set yl 'JK Error (average)'
plot 'n16s8t07_improved_energy_binning.dat' u 1:3  w l t 'L=8',\
     'n16s12t07_improved_energy_binning.dat' u 1:3 w l t 'L=12',\
     'n16s16t07_improved_energy_binning.dat' u 1:3 w l t 'L=16',\
#     'n16s24t07_improved_energy_binning.dat' u 1:3 w l t 'L=24',\
     'n16s32t07_improved_energy_binning.dat' u 1:3 w l t 'L=32'
unset multiplot
#
################################
# create png files
set term png enhanced
set out 't07_unimproved_energy_thermalization.png'
set title "Thermalization study T=0.7 unimproved"
set xl 'Number of CUT trajectories'
set yl 'Energy (average and JK errors)'
plot 'n16s32t07_unimproved_energy_thermalization.dat' u 1:2:3 w yerr t 'L=32'
set out 't07_unimproved_energy_stability.png'
set title "Stability study T=0.7 unimproved"
set xl 'Number of LAST trajectories'
set yl 'Energy (average and JK errors)'
plot 'n16s32t07_unimproved_energy_stability.dat' u 1:2:3 w yerr t 'L=32'
set out 't07_improved_energy_thermalization.png'
set title "Thermalization study T=0.7 improved"
set xl 'Number of CUT trajectories'
set yl 'Energy (average and JK errors)'
plot 'n16s8t07_improved_energy_thermalization.dat' u 1:2:3 w yerr t 'L=8',\
     'n16s12t07_improved_energy_thermalization.dat' u 1:2:3 w yerr t 'L=12',\
     'n16s16t07_improved_energy_thermalization.dat' u 1:2:3 w yerr t 'L=16',\
     'n16s24t07_improved_energy_thermalization.dat' u 1:2:3 w yerr t 'L=24',\
     'n16s32t07_improved_energy_thermalization.dat' u 1:2:3 w yerr t 'L=32'
set out 't07_improved_energy_stability.png'
set title "Stability study T=0.7 improved"
set xl 'Number of LAST trajectories'
set yl 'Energy (average and JK errors)'
plot 'n16s8t07_improved_energy_stability.dat' u 1:2:3 w yerr t 'L=8',\
     'n16s12t07_improved_energy_stability.dat' u 1:2:3 w yerr t 'L=12',\
     'n16s16t07_improved_energy_stability.dat' u 1:2:3 w yerr t 'L=16',\
     'n16s24t07_improved_energy_stability.dat' u 1:2:3 w yerr t 'L=24',\
     'n16s32t07_improved_energy_stability.dat' u 1:2:3 w yerr t 'L=32'
set out 't07_improved_energy_binning.png'
set title "Binning study T=0.7 improved"
set xl 'Number of measures in one BIN'
set yl 'JK Error (average)'
plot 'n16s8t07_improved_energy_binning.dat' u 1:3  w l t 'L=8',\
     'n16s12t07_improved_energy_binning.dat' u 1:3 w l t 'L=12',\
     'n16s16t07_improved_energy_binning.dat' u 1:3 w l t 'L=16',\
#     'n16s24t07_improved_energy_binning.dat' u 1:3 w l t 'L=24',\
     'n16s32t07_improved_energy_binning.dat' u 1:3 w l t 'L=32'
####
