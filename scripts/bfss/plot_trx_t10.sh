#!/usr/local/bin/gnuplot
set term x11 persist
set multiplot layout 2, 2 font ",14"
set title "Thermalization study T=1.0 unimproved"
set xl 'Number of CUT trajectories'
set yl 'Tr[X2] (average and JK errors)'
plot 'n16s16t10_unimproved_trx_thermalization.dat' u 1:2:3 w yerr t 'L=16',\
     'n16s24t10_unimproved_trx_thermalization.dat' u 1:2:3 w yerr t 'L=24',\
     'n16s32t10_unimproved_trx_thermalization.dat' u 1:2:3 w yerr t 'L=32',\
    'n16s32t10b_unimproved_trx_thermalization.dat' u 1:2:3 w yerr t 'L=32b',\
     'n16s48t10_unimproved_trx_thermalization.dat' u 1:2:3 w yerr t 'L=48',\
     'n16s64t10_unimproved_trx_thermalization.dat' u 1:2:3 w yerr t 'L=64'
set title "Stability study T=1.0 unimproved"
set xl 'Number of LAST trajectories'
set yl 'Tr[X2] (average and JK errors)'
plot 'n16s16t10_unimproved_trx_stability.dat' u 1:2:3 w yerr t 'L=16',\
     'n16s24t10_unimproved_trx_stability.dat' u 1:2:3 w yerr t 'L=24',\
     'n16s32t10_unimproved_trx_stability.dat' u 1:2:3 w yerr t 'L=32',\
    'n16s32t10b_unimproved_trx_stability.dat' u 1:2:3 w yerr t 'L=32b',\
     'n16s48t10_unimproved_trx_stability.dat' u 1:2:3 w yerr t 'L=48',\
     'n16s64t10_unimproved_trx_stability.dat' u 1:2:3 w yerr t 'L=64'
set title "Thermalization study T=1.0 improved"
set xl 'Number of CUT trajectories'
set yl 'Tr[X2] (average and JK errors)'
plot 'n16s16t10_improved_trx_thermalization.dat' u 1:2:3 w yerr t 'L=16',\
     'n16s24t10_improved_trx_thermalization.dat' u 1:2:3 w yerr t 'L=24'
#     'n16s32t10_improved_trx_thermalization.dat' u 1:2:3 w yerr t 'L=32',\
#    'n16s32t10b_improved_trx_thermalization.dat' u 1:2:3 w yerr t 'L=32b',\
#     'n16s48t10_improved_trx_thermalization.dat' u 1:2:3 w yerr t 'L=48',\
#     'n16s64t10_improved_trx_thermalization.dat' u 1:2:3 w yerr t 'L=64'
set title "Stability study T=1.0 improved"
set xl 'Number of LAST trajectories'
set yl 'Tr[X2] (average and JK errors)'
plot 'n16s16t10_improved_trx_stability.dat' u 1:2:3 w yerr t 'L=16',\
     'n16s24t10_improved_trx_stability.dat' u 1:2:3 w yerr t 'L=24'
#     'n16s32t10_improved_trx_stability.dat' u 1:2:3 w yerr t 'L=32',\
#    'n16s32t10b_improved_trx_stability.dat' u 1:2:3 w yerr t 'L=32b',\
#     'n16s48t10_improved_trx_stability.dat' u 1:2:3 w yerr t 'L=48',\
#     'n16s64t10_improved_trx_stability.dat' u 1:2:3 w yerr t 'L=64'
unset multiplot
# create png files
set term png enhanced
set out 't10_unimproved_trx_thermalization.png'
set title "Thermalization study T=1.0"
set xl 'Number of CUT trajectories'
set yl 'Tr[X^2] (average and JK errors)'
plot 'n16s16t10_unimproved_trx_thermalization.dat' u 1:2:3 w yerr t 'L=16',\
     'n16s24t10_unimproved_trx_thermalization.dat' u 1:2:3 w yerr t 'L=24',\
     'n16s32t10_unimproved_trx_thermalization.dat' u 1:2:3 w yerr t 'L=32',\
    'n16s32t10b_unimproved_trx_thermalization.dat' u 1:2:3 w yerr t 'L=32b',\
     'n16s48t10_unimproved_trx_thermalization.dat' u 1:2:3 w yerr t 'L=48',\
     'n16s64t10_unimproved_trx_thermalization.dat' u 1:2:3 w yerr t 'L=64'
set out 't10_unimproved_trx_stability.png'
set title "Stability study T=1.0"
set xl 'Number of LAST trajectories'
set yl 'Tr[X^2] (average and JK errors)'
plot 'n16s16t10_unimproved_trx_stability.dat' u 1:2:3 w yerr t 'L=16',\
     'n16s24t10_unimproved_trx_stability.dat' u 1:2:3 w yerr t 'L=24',\
     'n16s32t10_unimproved_trx_stability.dat' u 1:2:3 w yerr t 'L=32',\
    'n16s32t10b_unimproved_trx_stability.dat' u 1:2:3 w yerr t 'L=32b',\
     'n16s48t10_unimproved_trx_stability.dat' u 1:2:3 w yerr t 'L=48',\
     'n16s64t10_unimproved_trx_stability.dat' u 1:2:3 w yerr t 'L=64'
set out 't10_improved_trx_thermalization.png'
set title "Thermalization study T=1.0"
set xl 'Number of CUT trajectories'
set yl 'Tr[X^2] (average and JK errors)'
plot 'n16s16t10_improved_trx_thermalization.dat' u 1:2:3 w yerr t 'L=16',\
     'n16s24t10_improved_trx_thermalization.dat' u 1:2:3 w yerr t 'L=24'
#     'n16s32t10_improved_trx_thermalization.dat' u 1:2:3 w yerr t 'L=32',\
#     'n16s48t10_improved_trx_thermalization.dat' u 1:2:3 w yerr t 'L=48',\
#     'n16s64t10_improved_trx_thermalization.dat' u 1:2:3 w yerr t 'L=64'
set out 't10_improved_trx_stability.png'
set title "Stability study T=1.0"
set xl 'Number of LAST trajectories'
set yl 'Tr[X^2] (average and JK errors)'
plot 'n16s16t10_improved_trx_stability.dat' u 1:2:3 w yerr t 'L=16',\
     'n16s24t10_improved_trx_stability.dat' u 1:2:3 w yerr t 'L=24'
#     'n16s32t10_improved_trx_stability.dat' u 1:2:3 w yerr t 'L=32',\
#     'n16s48t10_improved_trx_stability.dat' u 1:2:3 w yerr t 'L=48',\
#     'n16s64t10_improved_trx_stability.dat' u 1:2:3 w yerr t 'L=64'
####
