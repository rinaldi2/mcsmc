#!/bin/bash
###############################################################################
# this script creates the summary.tex file and compiles it to make summary.pdf
###############################################################################
# everything is in this directory
maindir="/Users/rinaldi2/Documents/Projects/Strings"
# notes are in this directory
notesdir="notes"
# summary plots are in this directory's sub-directories
filesdir="data_from_oslic"
# summary files are here
listnames="listsummaryfiles.txt"
# to generate the list from scratch run the followin line
(cd ${maindir}/${filesdir} && find */summary.pdf > ${listnames})
echo "There are $(wc -l ${maindir}/${filesdir}/${listnames} | ak 1) summary files"
# start the summary.tex file with the header
cat header.tex > summary.tex
# modify the figure template for each guy in the list
for file in `cat ${maindir}/${filesdir}/${listnames}` ; do
    N=$(echo $file | sed -e"s|S.*||;s|N||")
    L=$(echo $file | sed -e"s|.*S||;s|T.*||")
    T=$(echo $file | cut -d'_' -f1 | sed -e"s|.*T||")
    TT=$(echo $T | sed -e"s|\(.\)|\1.|;s|[a-z]||")
    stream=$(echo $file | cut -d'_' -f1 | sed -e"s|.*[0-9]||")
    if [[ -z "${stream}" ]] ; then stream="a" ; fi
    figurename="${maindir}/${filesdir}/${file}"
    dirname=${file%%/summary.pdf}
    echo $N $L $T $TT $stream
    sed -e"s|XFFX|${figurename}|;\
    	   s|XCCX|N=${N} L=${L} T=${TT} Stream=${stream}|;\
	   s|XLLX|${dirname}|" figure_template.tex.tmp > figure_${dirname}.tex
# input every figure into the summary.tex
    echo "\input{figure_${dirname}.tex}" >> summary.tex
done
# finish the tex document
cat tail.tex >> summary.tex
# compile the document
pdflatex summary.tex
pdflatex summary.tex
# open the summary PDF file
open summary.pdf
