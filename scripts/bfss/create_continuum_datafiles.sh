#!/bin/bash
# create datafiles for continuum values of the energy taken from the fit logs.
source ~/.bash_colors
echo -e "${BBlue} ======================================== GRABBING CONTINUUM DATA ====================================== ${Color_Off}"
# select cut and statistics
if [[ $# -ne 2 ]] ; then
    echo "Usage: `basename $0` <CUT> <SUFF>" && exit
fi
cut=$1
suff=$2
####
# Function that extracts data from fit log files
get_values_and_error () {
    if [[ $# -ne 1 ]] ; then
        echo "Function usage: get_values_and_error <log file>" && exit 1
    fi
    filename=$1
    if [[ ! -f $filename ]] ; then echo "File $filename does not exist!" && exit ; fi

    target=""
    if [ $(echo $filename | grep linear) ] ; then target="E0" ; fi
    if [ $(echo $filename | grep quadratic) ] ; then target="EE0" ; fi
    if [ $(echo $filename | grep -e"2d_f") ] ; then target="E00" ; fi
    if [ $(echo $filename | grep -e"2d_g") ] ; then target="EE00" ; fi
    
    if [[ -z "${target}" ]] ; then echo "Target is not defined" && exit ; fi

    
    values=$(awk -v x=$t -v y=$target '$1==y && $2=="=" {print x/10,$3,$5}' $filename)
    dof=$(cat $filename | grep -e"FIT_NDF" | ak 6)
    chisquare=$(cat $filename | grep -e"FIT_STDFIT" | ak 8)
    redchisquare=$(cat $filename | grep variance | ak 9)

    echo "$values # $dof $chisquare | $redchisquare"
    return 0
}
#
write_header () {
    echo "# t energy error # dof chisq | red_chisq"
    return 0
}
#
#debug
# n=16
# t=09
# echo "# t energy error # dof chisq | red_chisq "
# get_values_and_error fit_continuum_linear_n${n}t${t}_energy_improved_${cut}_${suff}.log
# exit
#debug
#
#
#####################
# We should loop over N and T
#
nlist="16 24"
tlist="05 06 07 08 09 10"
#
#
# data from 1d fits: fixed N and T
rm -f ../data_avg/continuum_n*_linear.dat ../data_avg/continuum_n*_quadratic.dat ../data_avg/continuum_largeN_2d_f.dat ../data_avg/continuum_largeN_2d_g.dat
#
for n in $nlist ; do
    write_header > ../data_avg/continuum_n${n}_linear.dat
    for t in $tlist ; do
         get_values_and_error fit_continuum_linear_n${n}t${t}_energy_improved_${cut}_${suff}.log >> ../data_avg/continuum_n${n}_linear.dat
    done
done
#
for n in $nlist ; do
    write_header > ../data_avg/continuum_n${n}_quadratic.dat
    for t in $tlist ; do
        get_values_and_error fit_continuum_quadratic_n${n}t${t}_energy_improved_${cut}_${suff}.log >> ../data_avg/continuum_n${n}_quadratic.dat
    done
done
# data from 2d fits: fixed T
write_header > ../data_avg/continuum_largeN_2d_f.dat
for t in $tlist ; do
    get_values_and_error fit_continuum_2d_f_t${t}_energy_improved_${cut}_${suff}.log >> ../data_avg/continuum_largeN_2d_f.dat
done
#
write_header > ../data_avg/continuum_largeN_2d_g.dat
for t in $tlist ; do
    get_values_and_error fit_continuum_2d_g_t${t}_energy_improved_${cut}_${suff}.log >> ../data_avg/continuum_largeN_2d_g.dat
done
