#!/usr/local/bin/gnuplot
set term x11 persist
set multiplot layout 1, 2 font ",14"
set title "Thermalization study T=0.6 improved"
set xl 'Number of CUT trajectories'
set yl 'Energy (average and JK errors)'
plot 'n16s8t06_improved_energy_thermalization.dat' u 1:2:3 w yerr t 'L=8',\
     'n16s12t06_improved_energy_thermalization.dat' u 1:2:3 w yerr t 'L=12',\
     'n16s16t06_improved_energy_thermalization.dat' u 1:2:3 w yerr t 'L=16',\
     'n16s24t06_improved_energy_thermalization.dat' u 1:2:3 w yerr t 'L=24',\
     'n16s32t06_improved_energy_thermalization.dat' u 1:2:3 w yerr t 'L=32'
set title "Stability study T=0.6 improved"
set xl 'Number of LAST trajectories'
set yl 'Energy (average and JK errors)'
plot 'n16s8t06_improved_energy_stability.dat' u 1:2:3 w yerr t 'L=8',\
     'n16s12t06_improved_energy_stability.dat' u 1:2:3 w yerr t 'L=12',\
     'n16s16t06_improved_energy_stability.dat' u 1:2:3 w yerr t 'L=16',\
     'n16s24t06_improved_energy_stability.dat' u 1:2:3 w yerr t 'L=24',\
     'n16s32t06_improved_energy_stability.dat' u 1:2:3 w yerr t 'L=32'
unset multiplot
#
################################
# create png files
set term png enhanced
set out 't06_improved_energy_thermalization.png'
set title "Thermalization study T=0.6 improved"
set xl 'Number of CUT trajectories'
set yl 'Energy (average and JK errors)'
plot 'n16s8t06_improved_energy_thermalization.dat' u 1:2:3 w yerr t 'L=8',\
     'n16s12t06_improved_energy_thermalization.dat' u 1:2:3 w yerr t 'L=12',\
     'n16s16t06_improved_energy_thermalization.dat' u 1:2:3 w yerr t 'L=16',\
     'n16s24t06_improved_energy_thermalization.dat' u 1:2:3 w yerr t 'L=24',\
     'n16s32t06_improved_energy_thermalization.dat' u 1:2:3 w yerr t 'L=32'
set out 't06_improved_energy_stability.png'
set title "Stability study T=0.6 improved"
set xl 'Number of LAST trajectories'
set yl 'Energy (average and JK errors)'
plot 'n16s8t06_improved_energy_stability.dat' u 1:2:3 w yerr t 'L=8',\
     'n16s12t06_improved_energy_stability.dat' u 1:2:3 w yerr t 'L=12',\
     'n16s16t06_improved_energy_stability.dat' u 1:2:3 w yerr t 'L=16',\
     'n16s24t06_improved_energy_stability.dat' u 1:2:3 w yerr t 'L=24',\
     'n16s32t06_improved_energy_stability.dat' u 1:2:3 w yerr t 'L=32'
####
