#!/bin/bash
# steps to do for a complete analysis starting from the bare MC history files
#
############################################################
# CHOOSE first traj and last traj for the analysis
############################################################
cut=1000
last="all"
#
#
# Test good output
testing () {  [ ! $? -eq 0 ] && echo "Something is wrong. Exit" && exit 1; }
#
#
# Step 1: download the latest data files from oslic.llnl.gov into 'data_from_oslic' (skip it if you just want to do a different cut and last)
#./sync_measurements.sh
#testing
#
#
# Step 1 bis: merge some multi-stream runs
#cd data_from_oslic || exit
#./merge_streams.sh
#cd ..
#
#
# Step 2: average all the files into 'data_avg'
#cd data_avg || exit
#./avg_files.sh $cut $last | tee log_${cut}_${last}.log
#cd ..
#
#
# Step 3: do the continuum limit fits whose results are logged into 'fits' and plotted into 'plots'
./fit_all.sh $cut $last | tee logfit_${cut}_${last}.log
testing
./fit_all_2d.sh $cut $last | tee logfit_2d_${cut}_${last}.log
testing
#
#
# Step 4: create the datafiles with continuum values taken from the fit logs for plotting with VEUSZ
cd fits || exit
./create_continuum_datafiles.sh $cut $last
cd ..
#
# Step 4bis: create complete dataset for Evan
cd data_avg || exit
./create_complete_dataset.sh
cd ..
#
#
# Step 5: create bundle PDF files to easily look at the png files in 'plots' coming from the fits
./montage_all.sh
# and from the runs
cd data_from_oslic || exit
./montage_all.sh
cd ../ || exit
#
#
# Step 6: create summary PDF files in latex into 'notes'
cd notes || exit
./create_summary.sh
./create_fit_summary.sh $cut $last
cd ../ || exit
#
#
# Step 7: push everything to the cloud!
./copy_plots_gdrive.sh
#
#
# THE END
