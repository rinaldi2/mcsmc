#!/bin/bash
source ~/.bash_colors
echo -e "${BBlue} ======================================== ANALYSIS ====================================== ${Color_Off}"

if [[ $# -ne 2 ]] ; then
    echo "Usage: `basename $0` <cut> <last> (use \"all\" for max statistics)" && exit
fi
cut=$1
suff=$2
# number of colors
N="12 16 24 32"
# temperatures
T="04 0425 045 0475 05 06 065 07 075 08 085 09 10"
# actions
A="unimproved improved"
# observables
O="energy pol trx trxy"
###############################
# loop over all possibilities
for ncol in $N ; do
    for temp in $T ; do
	     for obs in $O ; do
	        for action in $A ; do
		          ./create_avg_err_file.sh $ncol $temp $cut $suff $obs $action
	        done
	     done
    done
done
