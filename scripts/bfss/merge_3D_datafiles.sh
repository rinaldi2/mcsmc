#!/bin/bash
if [[ $# -ne 2 ]] ; then
    echo "Usage: `basename $0` <CUT> <SUFF>" && exit
fi
# suff should be all if you want the highest statistics
cut=$1
suff=$2
# change templist if needed
templist="05 06 065 07 075 08 085 09 10"
rm -f t*_energy_improved_${cut}_${suff}.dat
for t in $templist ; do 
    for file in `ls n*t${t}_energy_improved_${cut}_${suff}.dat` ; do 
	n=$(echo ${file} | sed -e"s|t.*||;s|n||")
	awk -v x=$n '$1~/[0-9]/ {print x,$1,$2,$3}' ${file} >> t${t}_energy_improved_${cut}_${suff}.dat
    done
done
