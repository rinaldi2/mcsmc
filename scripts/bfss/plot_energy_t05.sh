#!/usr/local/bin/gnuplot
set term x11 persist
set multiplot layout 1, 3 font ",14"
set title "Thermalization study T=0.5 improved"
set xl 'Number of CUT trajectories'
set yl 'Energy (average and JK errors)'
plot 'n16s8t05_improved_energy_thermalization.dat' u 1:2:3 w yerr t 'L=8',\
     'n16s12t05_improved_energy_thermalization.dat' u 1:2:3 w yerr t 'L=12',\
     'n16s16t05_improved_energy_thermalization.dat' u 1:2:3 w yerr t 'L=16',\
     'n16s24t05_improved_energy_thermalization.dat' u 1:2:3 w yerr t 'L=24',\
     'n16s32t05_improved_energy_thermalization.dat' u 1:2:3 w yerr t 'L=32'
set title "Stability study T=0.5 improved"
set xl 'Number of LAST trajectories'
set yl 'Energy (average and JK errors)'
plot 'n16s8t05_improved_energy_stability.dat' u 1:2:3 w yerr t 'L=8',\
     'n16s12t05_improved_energy_stability.dat' u 1:2:3 w yerr t 'L=12',\
     'n16s16t05_improved_energy_stability.dat' u 1:2:3 w yerr t 'L=16',\
     'n16s24t05_improved_energy_stability.dat' u 1:2:3 w yerr t 'L=24',\
     'n16s32t05_improved_energy_stability.dat' u 1:2:3 w yerr t 'L=32'
set title "Binning study T=0.5 improved"
set xl 'Number of measures in one BIN'
set yl 'JK Error (average)'
plot 'n16s8t05_improved_energy_binning.dat' u 1:3  w l t 'L=8',\
     'n16s12t05_improved_energy_binning.dat' u 1:3 w l t 'L=12',\
     'n16s16t05_improved_energy_binning.dat' u 1:3 w l t 'L=16',\
     'n16s24t05_improved_energy_binning.dat' u 1:3 w l t 'L=24',\
     'n16s32t05_improved_energy_binning.dat' u 1:3 w l t 'L=32'
unset multiplot
#
################################
# create png files
set term png enhanced
set out 't05_improved_energy_thermalization.png'
set title "Thermalization study T=0.5 improved"
set xl 'Number of CUT trajectories'
set yl 'Energy (average and JK errors)'
plot 'n16s8t05_improved_energy_thermalization.dat' u 1:2:3 w yerr t 'L=8',\
     'n16s12t05_improved_energy_thermalization.dat' u 1:2:3 w yerr t 'L=12',\
     'n16s16t05_improved_energy_thermalization.dat' u 1:2:3 w yerr t 'L=16',\
     'n16s24t05_improved_energy_thermalization.dat' u 1:2:3 w yerr t 'L=24',\
     'n16s32t05_improved_energy_thermalization.dat' u 1:2:3 w yerr t 'L=32'
set out 't05_improved_energy_stability.png'
set title "Stability study T=0.5 improved"
set xl 'Number of LAST trajectories'
set yl 'Energy (average and JK errors)'
plot 'n16s8t05_improved_energy_stability.dat' u 1:2:3 w yerr t 'L=8',\
     'n16s12t05_improved_energy_stability.dat' u 1:2:3 w yerr t 'L=12',\
     'n16s16t05_improved_energy_stability.dat' u 1:2:3 w yerr t 'L=16',\
     'n16s24t05_improved_energy_stability.dat' u 1:2:3 w yerr t 'L=24',\
     'n16s32t05_improved_energy_stability.dat' u 1:2:3 w yerr t 'L=32'
set out 't05_improved_energy_binning.png'
set title "Binning study T=0.5 improved"
set xl 'Number of measures in one BIN'
set yl 'JK Error (average)'
plot 'n16s8t05_improved_energy_binning.dat'  u 1:3 w l t 'L=8',\
     'n16s12t05_improved_energy_binning.dat' u 1:3 w l t 'L=12',\
     'n16s16t05_improved_energy_binning.dat' u 1:3 w l t 'L=16',\
     'n16s24t05_improved_energy_binning.dat' u 1:3 w l t 'L=24',\
     'n16s32t05_improved_energy_binning.dat' u 1:3 w l t 'L=32'
####
