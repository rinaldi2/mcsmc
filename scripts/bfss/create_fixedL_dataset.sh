for file in n32t*_energy_improved_1000_all.dat ; do
    t=$(echo $file | cut -d'_' -f1 | sed -e"s|n32t||")
    t=$( echo $t | sed -e"s|\(.\)|\1.|")
    stuff=$(awk '$1==16' $file)
    echo $t $stuff
done
