#!/bin/bash
rm -f complete_datatable.dat
rm -f datatable_*.dat
for obs in energy pol trx trxy ; do
    echo "Observable=${obs} ---"
    for action in improved unimproved ; do
        echo "-- Action=$action "
        for t in 04 05 06 065 07 075 08 085 09 10 ; do
            echo "--- Temperature=$t"
            for file in n*t${t}_${obs}_${action}_1000_all.dat ; do
                if [[ ! -f $file ]] ; then continue ; fi
                n=$(echo $file | sed -e"s|t.*||;s|n||")
                echo ----- $n $t $file
                awk -v n=$n -v t=$t -v a=$action '$1~/[0-9]/ {print t/10," & ",n," & ",$1," & ",a," & ",$5," & ",$2," & ",$3}' $file >> datatable_${obs}_${action}.dat
                echo -----------------
            done
        done
    done
done
paste -d ' ' datatable_energy_improved.dat datatable_pol_improved.dat datatable_trx_improved.dat datatable_trxy_improved.dat | ak 1,2,3,4,5,6,7,8,9,10,11,12,13,23,24,25,26,36,37,38,39,49,50,51,52,53 | grep -v nan > datatable_improved.dat
paste -d ' ' datatable_energy_unimproved.dat datatable_pol_unimproved.dat datatable_trx_unimproved.dat datatable_trxy_unimproved.dat | ak 1,2,3,4,5,6,7,8,9,10,11,12,13,23,24,25,26,36,37,38,39,49,50,51,52,53 | grep -v nan > datatable_unimproved.dat
