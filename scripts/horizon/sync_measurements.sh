#!/bin/bash
# syncing measurement files and plots from oslic.llnl.gov
source ~/.bash_colors
echo -e "${BBlue} ======================================== SYNC FROM OSLIC ====================================== ${Color_Off}"
#echo "Syncing from Oslic ..."
rsync -vaz oslic.llnl.gov:/g/g90/rinaldi2/Projects/horizonBHv1/listdirs.txt listdirs.txt
if [[ ! -f listdirs.txt ]] ; then echo "listdirs.txt not found" && exit 1 ; fi
for dir in `cat listdirs.txt` ; do
    echo -e "${BRed} sync $dir ${Color_Off}"
    #echo "    $dir"
    mkdir -p data_from_oslic/$dir
    rsync -vaz --files-from=listfiles oslic.llnl.gov:/g/g90/rinaldi2/Projects/horizonBHv1/${dir}/ data_from_oslic/${dir}/
done
echo -e "${BBlue} done ${Color_Off}"
