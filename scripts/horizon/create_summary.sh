#!/bin/bash
###############################################################################
# this script creates the summary.tex file and compiles it to make summary.pdf
###############################################################################
# everything is in this directory
maindir="/Users/rinaldi2/Documents/Projects/HorizonBHv1"
# notes are in this directory
# notesdir="notes"
# summary plots are in this directory's sub-directories
filesdir="data_from_oslic"
# summary files are here
listnames="listsummaryfiles.txt"
# to generate the list from scratch run the followin line
(cd ${maindir}/${filesdir} && find */summary.pdf > ${listnames})
echo "There are $(wc -l ${maindir}/${filesdir}/${listnames} | ak 1) summary files"
# start the summary.tex file with the header
cat header.tex > summary.tex
# start the table
echo "# Table of parameters" > table.txt
echo "# N L T R C action" >> table.txt
echo "Creating table of parameters..."
# modify the figure template for each guy in the list
for file in `cat ${maindir}/${filesdir}/${listnames}` ; do
    N=$(echo $file | sed -e"s|S.*||;s|N||")
    L=$(echo $file | sed -e"s|.*S||;s|T.*||")
    T=$(echo $file | sed -e"s|.*T||;s|R.*||")
    TT=$(echo $T | sed -e"s|\(.\)|\1.|")
    # probe position
    R=$(echo $file | sed -e"s|.*R||;s|C.*||")
    RR=$(echo "scale=1; ${R}/10" | bc)
    # curvature of the potential
    C=$(echo $file | cut -d'_' -f1 | sed -e"s|.*C||")
    # action: I used unimproved for a test on the bump
    A=$(echo $file | cut -d'_' -f2 | cut -d'/' -f1)
    # stream=$(echo $file | cut -d'_' -f1 | sed -e"s|.*[0-9]||")
    # if [[ -z "${stream}" ]] ; then stream="a" ; fi
    figurename="${maindir}/${filesdir}/${file}"
    dirname=${file%%/summary.pdf}
    echo $N $L $TT $RR $C $A >> table.txt
    sed -e"s|XFFX|${figurename}|;\
          s|XCCX|N=${N} L=${L} T=${TT} R=${RR} C=${C} A=${A}|;\
          s|XLLX|${dirname}|" figure_template.tex.tmp > figure_${dirname}.tex
# input every figure into the summary.tex
    echo "\input{figure_${dirname}.tex}" >> summary.tex
done
# finish the tex document
cat tail.tex >> summary.tex
#
echo "Compiling the tex file... it might take a while."
# compile the document
pdflatex summary.tex > output_texing.log
pdflatex summary.tex > output_texing2.log
# open the summary PDF file
open summary.pdf
