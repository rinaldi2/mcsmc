#!/bin/bash
#######################################################################
# this script creates sume bundle PDF files starting from png files   #
#######################################################################
for dir in N*S*T*R*C*_* ; do
    cd $dir || exit
    montage -density 300 -tile 2x0 -geometry +5+5 -border 2 cgit.png dtx.png energy.png trx.png trxbh.png offd.png r1.png r2.png summary.pdf
    cd ../ || exit
done
