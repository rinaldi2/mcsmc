#!/usr/local/bin/gnuplot
set term x11 persist
set multiplot layout 2,2 columnsfirst font "Arial,8"
set title "Thermalization study N=8 S=8 T=2.0 R=7.5 improved"
set xl 'Number of CUT trajectories'
set yl 'Distance (average and JK errors)'
plot 'n8s8t20r75c50_improved_distance_thermalization.dat' u 1:2:3 w yerr t 'C=50',\
     'n8s8t20r75c75_improved_distance_thermalization.dat' u 1:2:3 w yerr t 'C=75',\
     'n8s8t20r75c100_improved_distance_thermalization.dat' u 1:2:3 w yerr t 'C=100',\
     'n8s8t20r75c50_unimproved_distance_thermalization.dat' u 1:2:3 w yerr t 'unimproved C=50'
    #  'n8s8t20r75c50_improved_distance_thermalization.dat' u 1:2:3 w yerr t 'R=7.5',\
    #  'n8s8t20r80c50_improved_distance_thermalization.dat' u 1:2:3 w yerr t 'R=8',\
    #  'n8s8t20r90c50_improved_distance_thermalization.dat' u 1:2:3 w yerr t 'R=9'
set title "Stability study N=8 S=8 T=2.0 R=7.5 improved"
set xl 'Number of LAST trajectories'
set yl 'Distance (average and JK errors)'
plot 'n8s8t20r75c50_improved_distance_stability.dat' u 1:2:3 w yerr t 'C=50',\
     'n8s8t20r75c75_improved_distance_stability.dat' u 1:2:3 w yerr t 'C=75',\
     'n8s8t20r75c100_improved_distance_stability.dat' u 1:2:3 w yerr t 'C=100',\
     'n8s8t20r75c50_unimproved_distance_stability.dat' u 1:2:3 w yerr t 'unimproved C=50'
    #  'n8s8t20r75c50_improved_distance_stability.dat' u 1:2:3 w yerr t 'R=7.5',\
    #  'n8s8t20r80c50_improved_distance_stability.dat' u 1:2:3 w yerr t 'R=8',\
    #  'n8s8t20r90c50_improved_distance_stability.dat' u 1:2:3 w yerr t 'R=9'
set title "Binning study N=8 S=8 T=2.0 R=7.5 improved"
set xl 'Number of measures in one BIN'
set yl 'JK Error (average)'
plot 'n8s8t20r75c50_improved_distance_binning.dat' u 1:3 w l t 'C=50',\
     'n8s8t20r75c75_improved_distance_binning.dat' u 1:3 w l t 'C=75',\
     'n8s8t20r75c100_improved_distance_binning.dat' u 1:3 w l t 'C=100',\
     'n8s8t20r75c50_unimproved_distance_binning.dat' u 1:3 w l t 'unimproved C=50'
    #  'n8s8t20r75c50_improved_distance_binning.dat' u 1:2:3 w yerr t 'R=7.5',\
    #  'n8s8t20r80c50_improved_distance_binning.dat' u 1:2:3 w yerr t 'R=8',\
    #  'n8s8t20r90c50_improved_distance_binning.dat' u 1:2:3 w yerr t 'R=9'
set title "Sliding study N=8 S=8 T=2.0 R=7.5 improved"
set xl 'Starting trajectory'
set yl 'Distance (average and JK errors)'
plot 'n8s8t20r75c50_improved_distance_sliding.dat' u 1:2:3 w yerr t 'C=50',\
     'n8s8t20r75c75_improved_distance_sliding.dat' u 1:2:3 w yerr t 'C=75',\
     'n8s8t20r75c100_improved_distance_sliding.dat' u 1:2:3 w yerr t 'C=100',\
     'n8s8t20r75c50_unimproved_distance_sliding.dat' u 1:2:3 w yerr t 'unimproved C=50'
    #  'n8s8t20r75c50_improved_distance_sliding.dat' u 1:2:3 w yerr t 'R=7.5',\
    #  'n8s8t20r80c50_improved_distance_sliding.dat' u 1:2:3 w yerr t 'R=8',\
    #  'n8s8t20r90c50_improved_distance_sliding.dat' u 1:2:3 w yerr t 'R=9'
unset multiplot
#
################################
# create png files
# set term png enhanced
# set out 't10_unimproved_energy_thermalization.png'
# set title "Thermalization study T=1.0 unimproved"
# set xl 'Number of CUT trajectories'
# set yl 'Energy (average and JK errors)'
# plot 'n16s8t10_unimproved_energy_thermalization.dat' u 1:2:3 w yerr t 'L=8',\
#      'n16s16t10_unimproved_energy_thermalization.dat' u 1:2:3 w yerr t 'L=16',\
#      'n16s24t10_unimproved_energy_thermalization.dat' u 1:2:3 w yerr t 'L=24',\
#      'n16s32t10_unimproved_energy_thermalization.dat' u 1:2:3 w yerr t 'L=32'
####
