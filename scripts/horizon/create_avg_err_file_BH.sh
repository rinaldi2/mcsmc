#!/bin/bash
source ~/.bash_colors
# do analysis only if min_traj measurements are present
min_traj=1000
# usage:
if [[ $# -ne 7 ]] ; then
  echo "Usage: `basename $0` <N> <S> <T> <CUT> <TOT> <OBS> <ACTION>" && exit
fi
# set parameters from std input
N=$1
S=$2
T=$3
cut=$4
suff=$5
obs=$6
columns=19 # this is true for the trxbh files 
act=$7
fileavg="n${N}s${S}t${T}_${obs}_${act}_${cut}_${suff}.dat"
fileavg1="n${N}s${S}t${T}_v2_${act}_${cut}_${suff}.dat"
if [[ ! ${obs} = trxbh ]] ; then
  echo "This analysis script only works for Tr(X_BH). You selected observable=${obs}. I am quitting!" && exit 1
fi
datadir=$(pwd)/../data_from_oslic
distances=$(qls -d ${datadir}/N${N}S${S}T${T}R*C*_${act} | sed -e"s|.*R||;s|C.*||" | sort -n)
if [[ -z "${distances}" ]] ; then
  echo  -e "${Red}--- ${datadir}/N${N}S${S}T${T}R*C*_${act} is not a directory --- ${Color_Off}"
  exit 1
fi
echo "------------------------------------------------------------------------------------"
echo -n -e "${BBlue} Creating file $fileavg using the average value for the ${obs} for R= "
echo -e $distances ${Color_Off}
[[ ! "${suff}" == "all" ]] && echo -e "${BRed} fixing statistics! starting=${cut} ending=${suff} ${Color_Off}"
rm -f *_tmpavg.dat # auxiliary files to average multiple directions of trxbh for each distance
#
# Set headers for all files
for dir in `seq 1 9` ; do # this first loop is just to write out the header of different files
  file="n${N}s${S}t${T}_${obs}-${dir}_${act}_${cut}_${suff}.dat"
  echo -e "${BBlue} Creating file $file using the average value for dir=${dir} ${Color_Off}"
  echo "# R C ${obs}-${dir} err -> with ${cut} trajectories discarded for thermalization # included | bins" > $file
done
echo "# R C ${obs}-avg19 err ${obs}-avg29 err -> with ${cut} trajectories discarded for thermalization # included | bins" > $fileavg
#
#
# Do Monte Carlo average of Tr Xbh^2
for a in $distances ; do # loop over different distances of the probe
  anew=$(echo "scale=1; ${a}/10" | bc)
  # start loop over directions that append a new distance to each file
  for dir in `seq 1 9` ; do # loop over directions (dimensions of the matrix model)
    column=$[dir + 1]
    file="n${N}s${S}t${T}_${obs}-${dir}_${act}_${cut}_${suff}.dat"
    for FILE in ${datadir}/N${N}S${S}T${T}R${a}C*_${act}/${obs}.dat ; do # loop over different possible values of C
      b=$(echo $FILE | sed -e"s|.*C||;s|_.*||" )
      total=$(wc -l $FILE | ak 1)
      used=$[total-cut]
      if [[ "$used" -gt "${min_traj}" ]] ; then
        if [[ -f $FILE ]] ; then
          if [[ "${suff}" == "all" ]] ; then
            basic_analysis -n $FILE -c $columns -o $column -i ${cut} > tmpbuff
          else
            basic_analysis -n $FILE -c $columns -o $column -i ${cut} -f ${suff} > tmpbuff
          fi
          if grep Warning tmpbuff ; then
            echo -e "${Green} $FILE warning --- ${Color_Off}"
          fi
          echo -n $anew $b "$(grep -e"Obs->avr" tmpbuff | ak 2,3)" >> $file
          echo " # $(grep -e"Total points" tmpbuff | ak 4) | $(grep -e"Obs->numbins" tmpbuff | ak 2)" >> $file
          # save the average temporarily in a separate file for each R,C and direction
          echo "$(grep -e"Obs->avr" tmpbuff | ak 2,3)" >> ${a}_${b}_tmpavg.dat
        fi
      else
        echo "R=$anew and C=$b does not have at least ${min_traj} configurations ... "
      fi
    done
  done
  # for each distance calculate the average of TrxBH over the 9 dimensions (1-9 and 2-9)
  for tmpfile in ${a}_*_tmpavg.dat ; do
    c=$(echo $tmpfile | cut -d'_' -f2)
    avg19="$(awk 'BEGIN {x=0;y=0} {x+=$1; y+=$2*$2} END {print x/9,sqrt(y)/9}' $tmpfile)"
    avg29="$(tail -n8 $tmpfile | awk 'BEGIN {x=0;y=0} {x+=$1; y+=$2*$2} END {print x/8,sqrt(y)/8}')"
    echo $anew $c $avg19 $avg29 >> $fileavg
  done
done
#
#
# do Monte Carlo average for v^2 directions 1 to 9
rm -f *_tmpavg.dat
# Set headers for all files
for dir in `seq 1 9` ; do # this first loop is just to write out the header of different files
  file="n${N}s${S}t${T}_v-${dir}_${act}_${cut}_${suff}.dat"
  echo -e "${BBlue} Creating file $file using the average value for dir=${dir} ${Color_Off}"
  echo "# R C v-${dir} err -> with ${cut} trajectories discarded for thermalization # included | bins" > $file
done
echo "# R C v2-avg19 err v2-avg29 err -> with ${cut} trajectories discarded for thermalization # included | bins" > $fileavg1
#
#
# Do Monte Carlo average for v^2 directions 1 to 9
for a in $distances ; do
    anew=$(echo "scale=1; ${a}/10" | bc)
    for dir in `seq 1 9` ; do
        column=$[dir + 10]
        file="n${N}s${S}t${T}_v-${dir}_${act}_${cut}_${suff}.dat"
        for FILE in ${datadir}/N${N}S${S}T${T}R${a}C*_${act}/${obs}.dat ; do
            b=$(echo $FILE | sed -e"s|.*C||;s|_.*||" )
            total=$(wc -l $FILE | ak 1)
            used=$[total-cut]
            if [[ "$used" -gt "${min_traj}" ]] ; then
                if [[ -f $FILE ]] ; then
                    if [[ "${suff}" == "all" ]] ; then
                        basic_analysis -n $FILE -c $columns -o $column -i ${cut} > tmpbuff
                    else
                        basic_analysis -n $FILE -c $columns -o $column -i ${cut} -f ${suff} > tmpbuff
                    fi
                    if grep Warning tmpbuff ; then
                        echo -e "${Green} $FILE warning --- ${Color_Off}"
                    fi
                    echo -n $anew $b "$(grep -e"Obs->avr" tmpbuff | ak 2,3)" >> $file
                    echo " # $(grep -e"Total points" tmpbuff | ak 4) | $(grep -e"Obs->numbins" tmpbuff | ak 2)" >> $file
                    # save the average temporarily in a separate file for each R,C and direction
                    echo "$(grep -e"Obs->avr" tmpbuff | ak 2,3)" >> ${a}_${b}_tmpavg.dat
                fi
            else
                echo "R=$anew and C=$b does not have at least ${min_traj} configurations ... "
            fi
        done
    done
    # for each distance calculate the average of v over the 9 dimensions (1-9 and 2-9)
    for tmpfile in ${a}_*_tmpavg.dat ; do
        c=$(echo $tmpfile | cut -d'_' -f2)
        avg19="$(awk 'BEGIN {x=0;y=0} {x+=$1; y+=$2*$2} END {print x/9,sqrt(y)/9}' $tmpfile)"
        avg29="$(tail -n8 $tmpfile | awk 'BEGIN {x=0;y=0} {x+=$1; y+=$2*$2} END {print x/8,sqrt(y)/8}')"
        echo $anew $c $avg19 $avg29 >> $fileavg1
    done
done
rm -f *_tmpavg.dat # auxiliary files to average multiple directions of trxbh for each distance
