#!/bin/bash
source ~/.bash_colors
# do analysis only if min_traj measurements are present
min_traj=1000
# usage:
if [[ $# -ne 7 ]] ; then
    echo "Usage: `basename $0` <N> <S> <T> <CUT> <TOT> <OBS> <ACTION>" && exit
fi
# set parameters from std input
N=$1
S=$2
T=$3
cut=$4
suff=$5
obs=$6
columns=2
column=2
act=$7
file="n${N}s${S}t${T}_${obs}_${act}_${cut}_${suff}.dat"
if [[ ${obs} = distance ]] ; then
  columns=3
  column=2
  fileforce="n${N}s${S}t${T}_force_${act}_${cut}_${suff}.dat"
fi
datadir=$(pwd)/../data_from_oslic
distances=$(qls -d ${datadir}/N${N}S${S}T${T}R*C*_${act} | sed -e"s|.*R||;s|C.*||" | sort -n)
if [[ -z "${distances}" ]] ; then
    echo  -e "${Red}--- ${datadir}/N${N}S${S}T${T}R*C*_${act} is not a directory --- ${Color_Off}"
    exit 1
fi
echo "------------------------------------------------------------------------------------"
echo -n -e "${BBlue} Creating file $file using the average value for the ${obs} for R= "
echo -e $distances ${Color_Off}
[[ ! "${suff}" == "all" ]] && echo -e "${BRed} fixing statistics! starting=${cut} ending=${suff} ${Color_Off}"
echo "# R C ${obs} err -> with ${cut} trajectories discarded for thermalization # included | bins" > $file
for a in $distances ; do
  anew=$(echo "scale=1; ${a}/10" | bc)
  for FILE in ${datadir}/N${N}S${S}T${T}R${a}C*_${act}/${obs}.dat ; do
    b=$(echo $FILE | sed -e"s|.*C||;s|_.*||" )
    total=$(wc -l $FILE | ak 1)
    used=$[total-cut]
    if [[ "$used" -gt "${min_traj}" ]] ; then
      if [[ -f $FILE ]] ; then
  	    if [[ "${suff}" == "all" ]] ; then
  		      basic_analysis -n $FILE -c $columns -o $column -i ${cut} > tmpbuff
  	    else
  		      basic_analysis -n $FILE -c $columns -o $column -i ${cut} -f ${suff} > tmpbuff
  	    fi
  	    if grep Warning tmpbuff ; then
  		      echo -e "${Green} $FILE warning --- ${Color_Off}"
  	    fi
  	    echo -n $anew $b $(grep -e"Obs->avr" tmpbuff | ak 2,3) >> $file
  	    echo " # $(grep -e"Total points" tmpbuff | ak 4) | $(grep -e"Obs->numbins" tmpbuff | ak 2)" >> $file
  	  fi
    else
	    echo "R=$anew and C=$b does not have at least ${min_traj} configurations ... "
    fi
  done
done
# do the force if we just averaged the distance observable
if [[ ${obs} = distance ]] ; then
  echo -n -e "${BBlue} Creating file $fileforce using the average value for the ${obs} for R= "
  echo -e $distances ${Color_Off}
  echo "# R C force err -> with ${cut} trajectories discarded for thermalization # included | bins" > $fileforce
  awk '$1~/[0-9]/ {print $1,$2,(2*$2*($1-$3)),(2*$2*$4),$5,$6,$7,$8}' $file >> $fileforce
fi
