#!/bin/bash
###########
# works only
# in a limited
# number of cases
# (probe distance must be integer between 0 and 15 included)
############
# usage:
if [[ $# -ne 7 ]] ; then
    echo "Usage: `basename $0` <N> <S> <T> <CUT> <TOT> <OBS> <ACTION>" && exit
fi
# set parameters from std input
N=$1
S=$2
T=$3
cut=$4
suff=$5
obs=$6
act=$7
if [[ ! ${obs} = trxbh ]] ; then
    echo "This analysis script only works for Tr(X_BH), but observable=${obs}" && exit 1
fi
prefix="n${N}s${S}t${T}_${obs}"
suffix="${act}_${cut}_${suff}.dat"
file="n${N}s${S}t${T}_${obs}-29_${act}_${cut}_${suff}.dat"
echo "# R avg TrxBH dir 2 to 9 with err -> with ${cut} trajectories discarded for thermalization" > $file
rm -f *_f.dat
r=0
for dir in {2..9} ; do
  grep -e"^${r} " ${prefix}-${dir}_${suffix} | ak 3,4 >> ${r}_f.dat
done
echo "$r $(awk 'BEGIN {x=0;y=0} {x+=$1; y+=$2*$2} END {print x/8,sqrt(y)/8}' ${r}_f.dat)" >> $file
for r in {1..15} ; do
  for dir in {2..9} ; do
    grep -e"^${r}\.0" ${prefix}-${dir}_${suffix} | ak 3,4 >> ${r}_f.dat
  done
  echo "$r $(awk 'BEGIN {x=0;y=0} {x+=$1; y+=$2*$2} END {print x/8,sqrt(y)/8}' ${r}_f.dat)" >> $file
done
#rm -f *_f.dat
