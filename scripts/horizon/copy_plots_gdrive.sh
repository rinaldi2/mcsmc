#!/bin/bash
# syncing measurement files and plots to Google Drive
source ~/.bash_colors
echo -e "${BBlue} ======================================== SYNC PLOTS TO GDRIVE ====================================== ${Color_Off}"
cd data_from_oslic || exit
for dir in N*S*T*R*C* ; do
    echo -e "${BRed} sync $dir ${Color_Off}"
    mkdir -p ~/Google\ Drive/LLNL/BlackHorizon/${dir}
#    cp -p ${dir}/*.png ~/Google\ Drive/LLNL/BlackHorizon/${dir}/.
    cp -p ${dir}/summary.pdf ~/Google\ Drive/LLNL/BlackHorizon/${dir}/.
done
cd ../ || exit
echo -e "${BRed} sync plots ${Color_Off}"
cp data_avg/*.png plots/*.png data_thermalization/*.png *.png ~/Google\ Drive/LLNL/BlackHorizon/.
#echo -e "${BRed} sync fits ${Color_Off}"
#mkdir -p ~/Google\ Drive/LLNL/BlackStrings/fits
#cp -p fits/*.log ~/Google\ Drive/LLNL/BlackStrings/fits/.
echo -e "${BRed} sync data averages ${Color_Off}"
mkdir -p ~/Google\ Drive/LLNL/BlackHorizon/data
cp -p data_avg/*.dat ~/Google\ Drive/LLNL/BlackHorizon/data/.
echo -e "${BBlue} done ${Color_Off}"
