#!/bin/bash
source ~/.bash_colors
echo -e "${BBlue} ======================================== CREATE THERMALIZATION PLOTS ====================================== ${Color_Off}"
BASEDIR="/Users/rinaldi2/Documents/Projects/HorizonBHv1"
datadir=${BASEDIR}/data_from_oslic
#echo $(ls -d ${datadir}/N*S*T*R*C*_*) > listdirs.txt
echo "$(ls -d ${datadir}/N8S8T20R*C*_*)" > listdirs.txt
for obs in energy trx distance ; do
    for filein in `cat listdirs.txt` ; do
      fileout=$(echo ${filein##*oslic/}  | tr '[:upper:]' '[:lower:]')
      if [[ ${obs} = distance ]] ; then
        ./create_thermalization_file_distance.sh ${filein}/${obs}.dat ${fileout}_${obs}
      else
        ./create_thermalization_file.sh ${filein}/${obs}.dat ${fileout}_${obs}
      fi
    done
    echo -e "${BBlue} ------ done ${obs} ------ ${Color_Off}"
done
