#!/bin/bash
# steps to do for a complete analysis starting from the bare MC history files
#
############################################################
# CHOOSE first traj and last traj for the analysis
############################################################
cut=1000
last="all"
#last=12000
#
#
# Test good output
testing () {  [ ! $? -eq 0 ] && echo "Something is wrong. Exit" && exit 1; }
#
#
# Step 1: download the latest data files from oslic.llnl.gov into 'data_from_oslic' (skip it if you just want to do a different cut and last)
./sync_measurements.sh
testing
#
#
# Step 2: average all the files into 'data_avg'
cd data_avg || exit
./avg_files.sh $cut $last | tee averages_${cut}_${last}.log
cd ..
#
#
# Step 3: do the continuum limit fits whose results are logged into 'fits' and plotted into 'plots'
#./fit_all.sh $cut $last
#testing
#./fit_all_2d.sh $cut $last
#testing
#
#
# Step 4: create the datafiles with continuum values taken from the fit logs for plotting with VEUS
#cd fits
#./create_continuum_datafiles.sh $cut $last
#cd ..
#
#
# Step 5: create bundle PDF files to easily look at the png files in 'plots' coming from the fits
#./montage_all.sh
# and from the runs
(
  cd data_from_oslic || exit
  ./montage_all.sh
)
#
#
# Step 6: create summary PDF files in latex into 'notes'
(
  cd notes || exit
  ./create_summary.sh
#./create_fit_summary.sh $cut $last
)
#
#
# Step 7: push everything to the cloud!
./copy_plots_gdrive.sh
#
#
# THE END
