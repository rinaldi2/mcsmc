#!/bin/bash
source ~/.bash_colors
echo -e "${BBlue} ======================================== ANALYSIS ====================================== ${Color_Off}"

if [[ $# -ne 2 ]] ; then
    echo "Usage: `basename $0` <cut> <last> (use \"all\" for max statistics)" && exit
fi
cut=$1
suff=$2
#datadir=$(pwd)/../data_from_oslic
# number of colors
N="6 8 12 16"
#N="8 12"
# sizes
L="8 10"
# temperatures
T="10 15 20 30"
# actions
A="unimproved improved"
# observables
O="energy distance trxbh"
#O="trxbh"
###############################
# loop over all possibilities
for ncol in $N ; do
  for size in $L ; do
    for temp in $T ; do
      for obs in $O ; do
        if [[ ${obs} = trxbh ]] ; then
          for action in $A ; do
            ./create_avg_err_file_BH.sh $ncol $size $temp $cut $suff $obs $action
          done
        else
          for action in $A ; do
            ./create_avg_err_file.sh $ncol $size $temp $cut $suff $obs $action
          done
        fi
      done
    done
  done
done
